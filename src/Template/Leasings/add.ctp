<div class="headline">
		<div class="left"><h2><?php echo 'leases';?></h2></div>
		<div class="right"></div>
	</div>
	<div class="content-holder">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="leftnav" valign="top">
					<div id="arrow"></div>
					<?php 
						echo $this->element('admin_left_menu');
					?>
				</td>
				<td class="content" valign="top" width="99%">
					<div class="add-div">
	<div class="popup-div">

	<div class="pop-head"><?php echo 'create lease'." for ".$apartment_name;?></div>
	<?php 
		echo $this->Form->create('Leasings',
								array(
									'url' => array(
										'controller' =>'Leasings',
										'action' => 'add',
										'id' => $apartment_id
									),
									'inputDefaults' => array(
										'label' => false
									),
									'class' => 'popup-form', 
									'type'  => 'file'
								)
							);
	?>
	
    <ul>
			<li>
				
				<?php 
					echo $this->Form->input(
											'lease_start_date', 
											array( 
												'placeholder'=>'Lease Start Date',
												'label'=>'Lease Start Date',
												  'type' => 'date',
												 'minYear' => date('Y') -0,
                                                 'maxYear' => date('Y') + 10,
											)
										);
				?>
			</li>
            <li>
                
                <?php
				 echo $this->Form->input(
				                         'lease_end_date',
				                          array( 
												'placeholder'=>'Lease end Date',
												'label'=>'Lease End Date',
												  'type' => 'date',
												 'minYear' => date('Y') -0,
                                                 'maxYear' => date('Y') + 10,
											)
								   );
			   ?>
			</li>
			
         </ul>
		 <div class="btn-section">
			<div class="btns-div">
				<div class="float-rt">
         
					<?php 
						echo $this->Html->link(
												'Cancel', 
												array(
													'action'=>'index',
													$apartment_id),
													 
												array(
													'class' =>'cancel'
												)
											);
						echo $this->Form->submit(
												'Submit', 
												array(
													'div'=>false, 
													'class' => 'btn-org-s'
												)
											);
					?>
					</div>
			</div>
		</div>
	<?php 
		// form end
		echo $this->Form->end();
	?>


					</div>
				</div>
			</td>
		</tr>
	</table>
	</div>
	</div>
