		<div class="headline">
	<div class="left">
		<h2>
			<?php echo "Leases for ".$apartment_name;?>
		</h2>
	</div>
<div class="right">
	<?= $this->Html->link('Add Lease', ['action' => 'add','id'=>$apartment_id]) ?>
           </div>
 </div>
<div class="content-holder">
	<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td class="leftnav" valign="top">
				<div id="arrow"></div>
				<?php 
					echo $this->element('admin_left_menu');
				?>
			</td>
			<td class="content" valign="top" width="99%">
            <table cellpadding="0" cellspacing="1" width="98%" class="user-grid">
					<tr>
						<th>
							
															Lease Started Date
							
						</th>
						<th>
							
															Lease End Date
						</th>
                         <th>
							
						Actions
							
						</th>
                   </tr>
				  	<?php if ($leasings->isEmpty()): ?>
						<tr>
							<td colspan="2" class="center">
								<?php echo "Leases Not Found";?>
							</td>
						</tr>
						<?php else: ?>						
						<?php 
							$i = 1;
							foreach ($leasings as $leases) 
							{
						?>
								<tr <?php if($i%2 == 0) echo 'class="even"';?>>
									<td>
										<?php echo $leases->lease_start_date;?>
									</td>
									<td>
										<?php echo $leases->lease_end_date;?>
									</td>
                                    <td>
									 <div class="icons moduleicons">
                                      <?php
                                        echo $this->Html->link(
															   '<span class="icon-edit"></span>',
																			array(
																				'controller' =>'Leasings',
																				'action' => 'edit', 
																				'leaseid' => $leases->lease_id,
																				'id'=>$apartment_id
																		),
																			array(
																				'escape' => false, 
																				'title'=>'Edit Lease', 
																				
																			)
																		);
									  ?>
                                     </div>
								   </td>
						</tr>
						<?php 
								$i++;
							}
						?>		
					<?php endif; ?>
				</table>
			</td>
		</tr>
	</table>
   </div>	