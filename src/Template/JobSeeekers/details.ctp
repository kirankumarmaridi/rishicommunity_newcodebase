		<h2>
			<?php echo 'Details of "'.$jobseekername.'"';?>
		</h2>
	<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="99%">
				<?php if(!empty($jobseekers)) {?>
				<table cellpadding="0" cellspacing="1" width="98%">
					<tr>
						<td>Applied For</td>
						<td><?php echo $career_title;?></td>
					</tr>
					<tr>
						<td>Name</td>
						<td><?php echo $jobseekers->jobseeker_firstname." ".$jobseekers->jobseeker_lastname;?></td>
					</tr>
					<tr>
						<td>Email ID</td>
						<td><?php echo $jobseekers->jobseeker_email_id;?></td>
					</tr>
					<tr>
						<td>Mobile</td>
						<td><?php echo $jobseekers->jobseeker_mobilenumber;?></td>
					</tr>
					<tr>
						<td>Total Experience</td>
						<td><?php echo $jobseekers->jobseeker_total_experience;?></td>
					</tr>
					<tr>
						<td>Relevant Experience</td>
						<td><?php echo $jobseekers->jobseeker_relavent_experince;?></td>
					</tr>
					<tr>
						<td>Current CTC</td>
						<td><?php echo $jobseekers->jobseeker_current_ctc;?></td>
					</tr>
					<tr>
						<td>Expected CTC</td>
						<td><?php echo $jobseekers->jobseeker_expected_ctc;?></td>
					</tr>
					<tr>
						<td>Available Joining Date</td>
						<td><?php echo $jobseekers->jobseeker_available_joining_date;?></td>
					</tr>
					<tr>
						<td>Date of Birth</td>
						<td><?php echo $jobseekers->jobseeker_dob;?></td>
					</tr>
					<tr>
						<td>Current City</td>
						<td><?php echo $jobseekers->jobseeker_currentcity;?></td>
					</tr>
					<tr>
						<td>Highest Qualification</td>
						<td><?php echo $jobseekers->jobseeker_highest_qualification;?></td>
					</tr>
					<tr>
						<td>Resume</td>
						<td>
							<?php echo $this->Html->link('download', 
									                                           array( 
									                                                  'action' => 'download',
																					  'id' => $jobseekers->jobseeker_id
																			     )
															   ); ?>
						</td>
					</tr>
					<tr>
						<td>Photo</td>
						<td>
							<?php  echo $this->Html->image("/".PHOTOS_FOLDER."/".$jobseekers->jobseeker_photo, array('width' => '50px','height' => '50px','alt'=>'aswq'));?>
						</td>
					</tr>
				</table>
				<?php 
						echo $this->Html->link('Add Note', 
										array(
											'controller' => 'jobseekersadmincomments',
											'action' => 'add',
											'id' => $jobseekerid,
											'parentid' => 0
										)
									);
				?>
				<?php 
					$this->requestAction("jobseekersadmincomments/displayComments/".$jobseekerid."/0");
				?>
				<?php } else {
					echo "No Jobseeker found with this id";
				}?>
			</td>
		</tr>
	</table>