		<div class="headline">
	<div class="left">
		<h2>
			<?php echo 'Tenants';?>
		</h2>
	</div>
<div class="right">
 <?= $this->Html->link('Add New Tenant', ['action' => 'add']) ?>
				</div>
	</div>
 </div>
<div class="content-holder">
	<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td class="leftnav" valign="top">
				<div id="arrow"></div>
				<?php 
					echo $this->element('admin_left_menu');
				?>
			</td>
				<td class="content" valign="top" width="99%">
				<table cellpadding="0" cellspacing="1" width="98%" class="user-grid">
					<tr>
                        <th>
						
							Tenant First Name
						</th>
                         <th>
						  Tenant Last Name
						</th>
						 <th>
							Date Of Birth
						</th>
                             <th>
							 							
								Mobile No
						</th>
						<th>
							Email
						</th>
                        <th>
							Emergency Contact
						</th>
						<th>
						Current Location
						 </th>  
                        <th>
					   Actions
						</th>
					</tr>
					<?php if ($tenant->isEmpty()): ?>
						<tr>
							<td colspan="7" class="center">
								<?php echo 'Tenants Not Found';?>
							</td>
						</tr>
					<?php else: ?>						
						<?php 
							$i = 1;
							foreach ($tenant as $tenant) 
							{
						?>
								<tr <?php if($i%2 == 0) echo 'class="even"';?>>
									<td>
										<?php echo $tenant->tenant_firstname;?>
									</td>
									<td>
										<?php echo $tenant->tenant_lastname;?>
									</td>
									<td>
										<?php echo $tenant->tenant_date_of_birth;?>
									</td>
									<td>
										<?php echo $tenant->tenant_mobile;?>
									</td>
                                   <td>
										<?php echo $tenant->tenant_email;?>
									</td>
                                    <td>
										<?php echo $tenant->tenant_emergency_contact;?>
									</td>
									<td>
										<?php echo $tenant->tenant_current_location?>
									</td>
                                     <td>
									 <div class="icons moduleicons">
                                  
                                   		<?php echo $this->Html->link(
																			'View Details',
																		array(      'controller'=>'TenantAptData', 
																				'action' => 'view',
																				 $tenant->tenant_id
																			)
																		);?>
										 <?php echo $this->Html->link(
																			'Apartments',
																	    array(		  'controller'=>'TenantAptData',
																				 'action' => 'index',
																				  $tenant->tenant_id
																				),
																				array(
																				'escape' => false																		                                                                                )
																		);?>
																		
									<?php	echo $this->Html->link(
																			'<span class="icon-edit"></span>',
																			array(
																				'controller' =>'Tenants',
																				'action' => 'edit',
																				$tenant->tenant_id
																				),
																			array(
																				'escape' => false, 
																				'title'=>'Edit Tenant Details'
																			)
																		);
											                           ?>
									<?php	echo $this->Html->link(
																			'<span class="icon-delete"></span>',
																			array(
																				'controller' =>'Tenants',
																				'action' => 'delete',
																				$tenant->tenant_id
																				),
																			array(
																				'escape' => false, 
																				'title'=>'Delete Tenant Details'
																			)
																		);
											                           ?>			
																		
										 																																	
									</div>
									</td>
								</tr>
						<?php 
								$i++;
							}
						?>		
					<?php endif; ?>
			</table>
                <?php /*?><?php 
					if(!empty($tenants))
					{
						echo $this->element('pagination');
					}
				?><?php */?>	
			</td>
		</tr>
	</table>
   </div>	
 
   