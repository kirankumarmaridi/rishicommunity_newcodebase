<?php 
	echo $this->Html->css('jquery-ui');
	echo $this->Html->script("vendor/jquery-1.9.1.min");
	echo $this->Html->script("vendor/jquery-ui");
	echo $this->Html->script('jquery.validate');
	echo $this->Html->css('jquery.validate');	
	
?>
<script type="text/javascript">
$(function() {
    $("#datepicker").datepicker({ 
    	defaultDate: null,
    	changeMonth: true,
      	changeYear: true,
      	yearRange: '1900:' + new Date().getFullYear(),
      	dateFormat: "yy-mm-dd" 
    });    
});
</script>
	<div class="headline">
		<div class="left"><h2><?php echo 'Tenants';?></h2></div>
		<div class="right"></div>
	</div>
	<div class="content-holder">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="leftnav" valign="top">
					<div id="arrow"></div>
					<?php 
						echo $this->element('admin_left_menu');
					?>
				</td>
					<td class="content" valign="top" width="99%">
					<div class="add-div">		
	              <div class="popup-div">
                   <div class="pop-head"><?php echo 'EDit Tenant Details';?></div>
			<?php		
		echo $this->Form->create($tenant,
								array(
									
									'inputDefaults' => array(
										'label' => false
									),
									'class' => 'popup-form', 
									'type' => 'file'
								)
							);
							
	?>
    <ul>
			<li>
			
				<?php 
					echo $this->Form->input(
											'Tenants.tenant_firstname', 
											array( 
												'placeholder'=>'Tenant First Name',
												'label'=>'Tenant First Name'
											)
										);
				?>
			</li>
            <li>
              
                <?php
				 echo $this->Form->input('Tenants.tenant_lastname',
				                          array( 
												'placeholder'=>'Tenant last Name',
												 'type'       => 'text',
												 'label'=>'Tenant last Name'
											)
								   );
			   ?>
			</li>
			<li>
			
				<?php 
					echo $this->Form->input(
											'Tenants.tenant_email', 
											array( 
												'placeholder'=>'Tenant Email',
												  'label'=>'Tenant Email'
											)
										);	
				?>
			</li>
            <li>
		
				<?php 
					echo $this->Form->input(
											'Tenants.tenant_mobile', 
											array( 
											'placeholder'=>'Tenant Mobile',
											'label'=>'Tenant Mobile'
											)
										);	
				?>
			</li>	
            <li>
        
				<?php 
					echo $this->Form->input(
											'Tenants.tenant_emergency_contact', 
											array( 
												'placeholder'=>'Tenant Emergency Contact',
												'label'=>'Tenant Emergency Contact'
											)
										);	
				?>
              </li>
              <li>
				<?php 
					echo $this->Form->input(
											'Tenants.tenant_date_of_birth', 
											array( 
											     'type' => 'date',
												'placeholder'=>'Tenant Date Of Birth',
												'minYear' => date('Y') - 70,
                                                'maxYear' => date('Y') + 0,
												'label'=>'Tenant Date Of Birth'
												)
										);	
				?>
              </li>
              <li>	<?php 
					echo $this->Form->input(
											'Tenants.tenant_current_location', 
											array( 
												'placeholder'=>'Tenant Current Location',
												'label'=>'Tenant Current Location'
												
											)
										);	
				?>
              </li>
         </ul>
		    <div class="btn-section">
			<div class="btns-div">
				<div class="float-rt">
					<?php 
						echo $this->Html->link(
												'Cancel', 
												array(
													'action'=>'index'
												), 
												array(
													'class' =>'cancel'
												)
											);
						echo $this->Form->submit(
											 'Submit', 
												array(
													'div'=>false, 
													'class' => 'btn-org-s'
												)
											);
					?>
				</div>
			</div>
		</div>
	<?php 
		// form end
		echo $this->Form->end();
	?>
						</div>
				</div>
			</td>
		</tr>
	</table>
	</div>