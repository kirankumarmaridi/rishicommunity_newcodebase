<div class="headline">
	<div class="left">
		<h2>
			 STAFFUSERS
		</h2>
			</div>
	<div class="right">
	<?= $this->Html->link('Add New StaffUser', ['action' => 'add']); ?>          
	</div>
</div>
<div class="content-holder">
	<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td class="leftnav" valign="top">
				<div id="arrow"></div>
				<?php 
					echo $this->element('admin_left_menu');
				?>
			</td>    	<td class="content" valign="top" width="99%">
				<table cellpadding="0" cellspacing="1" width="98%" class="user-grid">
	
					<tr>
						<th>
							Staff User Id
						</th>
						<th>
							Staff User First Name
						</th>
						
						<th>
							Staff User Last Name
						</th>
						<th>
							Email
						</th>
						<th>
							Mobile
						</th>
						<th>
							ACTIONS
						</th>
					</tr>
					<?php if ($staffusers->isEmpty()): ?>
						<tr>
							<td colspan="4" class="center">
								<?php echo 'Staff User Not Found';?>
							</td>
						</tr>
					<?php else: ?>						
						<?php 
							$i = 1;
							foreach ($staffusers as $staffusers) 
							{
						?>
							<tr <?php if($i%2 == 0) echo 'class="even"';?>>
									<td>
										<?php echo $staffusers->admin_id ?>
									</td>
									<td>
										<?php echo $staffusers->admin_firstname ?>
									</td>
									<td>
										<?php echo $staffusers->admin_lastname ?>
									</td>
									<td>
										<?php echo $staffusers->admin_email ?>
									</td>
									<td>
										<?php echo $staffusers->admin_mobile ?>
									</td>
									<td>
									<div class="icons moduleicons">
											<?php 
												if($staffusers->admin_status==1 )
												{
													$image = '<span class="icon-arecord"></span>';
													$status = 'ACTIVE';
												} else {
													$image = '<span class="icon-drecord"></span>';
													$status = 'INACTIVE';	
												}
											?>
									
											<?php 
												echo $this->Html->link(
																			$image, 
																			array(
																				'controller' =>'StaffUsers',
																				'action'=>'changeStaffStatus', 
																				$staffusers->admin_id
																			),
																			array(
																				'escape' => false, 
																				'title' => $status
																			)
																		); 
											?>
										</div>
									</td>
								</tr>
						<?php 
								$i++;
							}
						?>		
					<?php endif; ?>
				</table>
				<?php 
					if(!empty($STAFFUSERS))
					{
						echo $this->element('pagination');
					}
				?>	
				<div class="icons" style="padding-right:18px;padding-left:0px;padding-bottom:10px;">	
					<div class="icon-grid">
						<span >
							<strong><?php echo 'Icon Represents' ?></strong>
						</span>
						<table cellpadding="0" cellspacing="1" width="98%" style="padding-top:10px;">
							<tr>
								<td>
									<span class="icon-arecord" style="cursor:default;"></span> - <?php echo 'Active' ?>
								</td>
								<td>
									<span class="icon-drecord"></span> - <?php echo 'InActive' ?>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</td>
		</tr>
	</table>	
</div>
                                 