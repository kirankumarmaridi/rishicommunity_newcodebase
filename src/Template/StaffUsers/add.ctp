<?php 
	echo $this->Html->css('jquery-ui');
	echo $this->Html->script("vendor/jquery-1.9.1.min");
	echo $this->Html->script("vendor/jquery-ui");
	echo $this->Html->script('jquery.validate');
	echo $this->Html->css('jquery.validate');	
	
?>
<script type="text/javascript">
$(function() {
    $("#datepicker").datepicker({ 
    	defaultDate: null,
    	changeMonth: true,
      	changeYear: true,
      	yearRange: '1900:' + new Date().getFullYear(),
      	dateFormat: "yy-mm-dd" 
    });    
});
</script>
	<div class="headline">
		<div class="left"><h2><?php echo 'StaffUsers';?></h2></div>
		<div class="right"></div>
	</div>
	<div class="content-holder">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="leftnav" valign="top">
					<div id="arrow"></div>
					<?php 
						echo $this->element('admin_left_menu');
					?>
				</td>
			<td class="content" valign="top" width="99%">
					<div class="add-div">

<div class="pop-head"><?php echo 'Create StaffUser';?></div>
	<?php 
		echo $this->Form->create('StaffUsers',
								array(
									'url' => array(
										'controller' =>'StaffUsers', 
										'action' => 'add'
									),
									'inputDefaults' => array(
										'label' => false
									),
									'class' => 'popup-form', 
									'type' => 'file'
								)
							);
	?>
	<ul>
    
			<li>
				
				<?php 
					
					echo $this->Form->input(
											'admin_firstname', 
											array( 
												'placeholder'=>'StaffUser First Name',
												'label'=>'First Name'
											)
										);
				?>
				
			</li>
            <li>
      
                <?php
				echo $this->Form->input(
											'admin_lastname', 
											array( 
												'placeholder'=>'StaffUser Last Name',
												'label'=>'Last Name'
											)
										);
			   ?>
			</li>
			<li>
				
				<?php 
					echo $this->Form->input(
											'admin_email', 
											array( 
												'placeholder'=>'StaffUser Email',
												'label'=>'Email'
											)
										);
				?>
			</li>
            <li>
				
				<?php 
					echo $this->Form->input(
											'admin_mobile', 
											array( 
												'placeholder'=>'StaffUser Mobile',
												'label'=>'Mobile'
											)
										);
				?>
				</ul>
				  <div class="btn-section">
			<div class="btns-div">
				<div class="float-rt">
			<?php 
						echo $this->Html->link(
												'Cancel', 
												array(
													'action'=>'index'), 
												array(
													'class' =>'cancel'
												)
											);
						echo $this->Form->submit(
												'Submit', 
												array(
													'div'=>false, 
													'class' => 'btn-org-s'
												)
											);
					?>
		</div>
			</div>
		</div>
	<?php 
		// form end
		echo $this->Form->end();
	?>

					</div>
				</div>
			</td>
		</tr>
	</table>