<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?php echo $this->Html->charset(); ?>
	<title><?=$this->fetch('title') ?></title>
		<?php
			echo $this->Html->css('styles');
			echo $this->Html->script('jquery-latest');
			
		?>
		<?=$this->fetch('script') ?>
		<script type="text/javascript">
			$(document).ready(function(){
				$(".acc-content").hide();
				$("h3.active").next(".acc-content").show();
				$("h3").click(function(){
					$(this).siblings(".acc-content").hide();
					$(this).addClass("active").siblings().removeClass("active");
					$(this).next(".acc-content").show();
				});	
			});
		</script>
	</head>

	<body>
		<!--page_wrapper start-->
		<div class="page_wrapper">
			<?php echo $this->element('admin_header');?>
			<!--User Group Content start here-->
			<div class="user-group">
				<div class="dashboard">
					<?php 
						if($this->request->session())
						{
							echo $this->element('breadcrumb');
						}
					?>
				</div>			
				<div class="content-div">
					</br>
					<div class="block-a"><h2>Admin</h2></div>
					<div class="block-a">
						<ul class="dashboard-view">
                        
                     
		<li>
			<?php echo $this->Html->link(
										'<span></span>Tenants', 
			   							array( 
											'controller' =>'Tenants', 
											'action' => 'index' 
										),
										array(
									  	    'class' => 'projects', 
									         'escape' => false, 
									         'title' => 'Tenants'
										)
									); 
			?>	
		</li><li>
			<?php echo $this->Html->link(
										'<span></span>Apartments', 
			   							array( 
											'controller' =>'Apartments', 
											'action' => 'index' 
										),
										array(
									  	    'class' => 'projects', 
									         'escape' => false, 
									         'title' => 'Apartments'
										)
									); 
			?>	
		</li>
		</ul>
		</div>
		<div class="block-a"><h2>System Configuration</h2></div>
					<div class="block-a">
						<ul class="dashboard-view">
		<li>
			<?php echo $this->Html->link(
										'<span></span>Tenantstates', 
			   							array( 
											'controller' =>'Tenantstates', 
											'action' => 'index' 
										),
										array(
									  	    'class' => 'projects', 
									         'escape' => false, 
									         'title' => 'Tenantstates'
										)
									); 
			?>	
		</li>
		<li>
			<?php echo $this->Html->link(
										'<span></span>BillTypes', 
			   							array( 
											'controller' =>'BillTypes', 
											'action' => 'index' 
										),
										array(
									  	    'class' => 'projects', 
									         'escape' => false, 
									         'title' => 'BillTypes'
										)
									); 
			?>	
		</li>
		<li>
			<?php echo $this->Html->link(
										'<span></span>DiscountCoupons', 
			   							array( 
											'controller' =>'DiscountCoupons', 
											'action' => 'index' 
										),
										array(
									  	    'class' => 'projects', 
									         'escape' => false, 
									         'title' => 'DiscountCoupons'
										)
									); 
			?>	
		</li>
		<li>
			<?php echo $this->Html->link(
										'<span></span>AccomodationType', 
			   							array( 
											'controller' =>'AccomodationType', 
											'action' => 'index' 
										),
										array(
									  	    'class' => 'projects', 
									         'escape' => false, 
									         'title' => 'AccomodationType'
										)
									); 
			?>	
		</li>
		</ul>
		</div>
		<div class="block-a"><h2>Site Content Management</h2></div>
					<div class="block-a">
						<ul class="dashboard-view">
		<li>
			<?php echo $this->Html->link(
										'<span></span>AboutUs', 
			   							array( 
											'controller' =>'Settings', 
											'action' => 'editabout',
											'id'=>1 
										),
										array(
									  	    'class' => 'projects', 
									         'escape' => false, 
									         'title' => 'AboutUs'
										)
									); 
			?>	
		</li>
		<li>
			<?php echo $this->Html->link(
										'<span></span>Contact', 
			   							array( 
											'controller' =>'Settings', 
											'action' => 'editcontact',
											'id'=>1
										),
										array(
									  	    'class' => 'projects', 
									         'escape' => false, 
									         'title' => 'Contact'
										)
									); 
			?>	
		</li>
		</ul>
		</div>
		<div class="block-a"><h2>My Profile</h2></div>
					<div class="block-a">
						<ul class="dashboard-view">
		<li>
			<?php echo $this->Html->link(
										'<span></span>Myprofile', 
			   							array( 
											'controller' =>'Admins', 
											'action' => 'changeprofile' 
										),
										array(
									  	    'class' => 'projects', 
									         'escape' => false, 
									         'title' => 'Myprofile'
										)
									); 
			?>	
		</li>
		<li>
			<?php echo $this->Html->link(
										'<span></span>ChangePassword', 
			   							array( 
											'controller' =>'Admins', 
											'action' => 'changepassword' 
										),
										array(
									  	    'class' => 'projects', 
									         'escape' => false, 
									         'title' => 'ChangePassword'
										)
									); 
			?>	
		</li>
		
		
	</ul>
					</div>
					<div class="block-a"><h2>System Configuration</h2></div>
					<div class="block-a">
						<ul class="dashboard-view">
		<li>
			<?php echo $this->Html->link(
										'<span></span>Tenantstates', 
			   							array( 
											'controller' =>'Tenantstates', 
											'action' => 'index' 
										),
										array(
									  	    'class' => 'projects', 
									         'escape' => false, 
									         'title' => 'Tenantstates'
										)
									); 
			?>	
		</li>
		<li>
			<?php echo $this->Html->link(
										'<span></span>BillTypes', 
			   							array( 
											'controller' =>'BillTypes', 
											'action' => 'index' 
										),
										array(
									  	    'class' => 'projects', 
									         'escape' => false, 
									         'title' => 'BillTypes'
										)
									); 
			?>	
		</li>
		<li>
			<?php echo $this->Html->link(
										'<span></span>DiscountCoupons', 
			   							array( 
											'controller' =>'DiscountCoupons', 
											'action' => 'index' 
										),
										array(
									  	    'class' => 'projects', 
									         'escape' => false, 
									         'title' => 'DiscountCoupons'
										)
									); 
			?>	
		</li>
		<li>
			<?php echo $this->Html->link(
										'<span></span>AccomodationType', 
			   							array( 
											'controller' =>'AccomodationType', 
											'action' => 'index' 
										),
										array(
									  	    'class' => 'projects', 
									         'escape' => false, 
									         'title' => 'AccomodationType'
										)
									); 
			?>	
		</li>
		</ul>
		</div>
		<div class="block-a"><h2>Careers</h2></div>
					<div class="block-a">
						<ul class="dashboard-view">
		<li>
			<?php echo $this->Html->link(
										'<span></span>StaffUsers', 
			   							array( 
											'controller' =>'StaffUsers', 
											'action' => 'index' 
										),
										array(
									  	    'class' => 'projects', 
									         'escape' => false, 
									         'title' => 'StaffUsers'
										)
									); 
			?>	
		</li>
		
		</ul>
		</div>
					
				</div>		
			</div>
			<!--User Group Content end here-->
			<div class="push"></div>
		</div>
		<!--page_wrapper end-->
		<?php echo $this->element('admin_footer');?>
	</body>
</html>