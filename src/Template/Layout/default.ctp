<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Rishi Community</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <?php
	echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('bootstrap-responsive.min');
		echo $this->Html->css('font-awesome.min');
		echo $this->Html->css('main');
		echo $this->Html->css('sl-slide');
		echo $this->Html->script("vendor/modernizr-2.6.2-respond-1.1.0.min");
		?>
</head>
<body>
     <!--Header-->
    <?php echo $this->element('header');?>

<section class="main-info">
    <div class="container">
       <?= $this->fetch('content') ?>
    </div>
</section>
		<!--page_wrapper end-->
		<?php echo $this->element('footer');?>		
	</body>
</html>