<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=8" />
		
		<?php echo $this->Html->charset(); ?>
		<title>  <?= $this->fetch('title') ?></title>
		<?php
			echo $this->Html->css('styles');		
			echo $this->Html->script('prototype');
			echo $this->Html->script('effects');
			echo $this->Html->script('modalbox');
			echo $this->Html->css('modalbox');
			
		?>	
		<?=$this->fetch('scripts')?>
		
		<?php 
			$controller = $this->request->param('controller');
			$action = $this->request->param('action');			
		?>		
		<style>
			.ui-menu { width: 100%; }
		</style>
	
	</head>
	<body>
		<!--page_wrapper start-->
		<div class="page_wrapper">				
			<?php echo $this->element('admin_header');?>
			<!--User Group Content start here-->
				<div class="user-group">
					<div class="dashboard">
						<?php 
							if($this->request->session())
							{
								echo $this->element('breadcrumb');
							}
						?>
						<div id="errormsg-default" align="center">
							<?php echo " ";?>
							<?php echo $this->Flash->render(); ?>
						</div>
					</div>							
					 <?= $this->fetch('content') ?>
				</div>
				
			<!--User Group Content end here-->
			<div class="push"></div>
		</div>
		<!--page_wrapper end-->
		<?php echo $this->element('admin_footer');?>		
	</body>
</html>
