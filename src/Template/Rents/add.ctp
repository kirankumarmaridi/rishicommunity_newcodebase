<?php 

	echo $this->Html->css('jquery-ui');
	echo $this->Html->script('vendor/jquery-1.9.1.min');
	echo $this->Html->script('vendor/jquery-ui');
?>
<style>
.datepicker{z-index:11151 !important;}
</style>
<script type="text/javascript" src="/rishicommunity/js/vendor/jquery-1.9.1.min.js">
<script type="text/javascript" src="/rishicommunity/js/vendor/jquery-ui.js">

<script>
$(function() {
    $("#datepicker").datepicker({ dateFormat: "yy-mm-dd" });

        
});
</script><div class="headline">
		<div class="left"><h2><?php echo 'Rents';?></h2></div>
		<div class="right"></div>
	</div>
	<div class="content-holder">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="leftnav" valign="top">
					<div id="arrow"></div>
					<?php 
						echo $this->element('admin_left_menu');
					?>
				</td>
				<td class="content" valign="top" width="99%">
					<div class="add-div">
					<div class="popup-div">
					<div class="pop-head"><?php echo 'Create Rent'." for ".$apartment_name;?></div>
	<?php 
		echo $this->Form->create('Rents',
								array(
									'url' => array(
										'controller' =>'Rents', 
										'action' => 'add',
										'id' => $apartment_id
									),
									'inputDefaults' => array(
										'label' => false
									),
									'class' => 'popup-form', 
									'type'  => 'file'
								))
	?>

	<?php /*?><?php   
					echo $this->Form->input(
											'apt_id', 
											array( 
												'type' => 'hidden',
												'value' => $apartment_id
											)
										);
				?><?php */?>

		<ul>
           <li>
				<?php 
					echo $this->Form->input(
											'rent_amount', 
											array('label'=>'Rent Amount'
											)
										);
				?>
			</li>
			<li>
				<?php 
					echo $this->Form->input(
											'rent_from_date', 
											array( 
												'lable'=>'Rent From Date',
												'type'        => 'date',
												'id' => 'datepicker'
											)
										);	
				?>
			</li>
           </ul>
		<div class="btn-section">
			<div class="btns-div">
				<div class="float-rt">
		
					<?php 
						echo $this->Html->link(
											'Cancel', 
												array(
													'action'=>'index',					
												   'id' => $apartment_id
												), 
												array(
													'class' =>'cancel'
												)
											);
											
						echo $this->Form->submit(
												'Submit', 
											array(
													'div'=>false, 
													'class' => 'btn-org-s'
												)
											);
					?>
			
				</div>
			</div>
		</div>
	<?php 
		// form end
		echo $this->Form->end();
	?>
						</div>
				</div>
			</td>
		</tr>
	</table>
	</div>
	</div>