<div class="headline">
		<div class="left"><h2><?php echo 'Rents';?></h2></div>
		<div class="right"></div>
	</div>
	<div class="content-holder">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="leftnav" valign="top">
					<div id="arrow"></div>
					<?php 
						echo $this->element('admin_left_menu');
					?>
				</td>
				<td class="content" valign="top" width="99%">
					<div class="add-div">
   	<div class="popup-div">
	<div class="pop-head"><?php echo 'Edit Rent Details'." for ".$apartment_name;?></div>

		<?php 
		echo $this->Form->create($rents,
		                        array(
								'url' => array(
										'controller' =>'Rents', 
										'action' => 'edit',
										'id' => $apartment_id,
										  'rentid'=>$rents->rent_id,
								),
		                         'inputDefaults' => array(
										'label' => false
									),
									'class' => 'popup-form', 
									'type' => 'file'
								)
							);	
							echo $this->Form->hidden('Rents.rent_id');
								
		?>

		<ul>
           <li>
				
				<?php 
					echo $this->Form->input(
											'Rents.rent_amount', 
											array('label'=>'Rent Amount',
											));

			?>
			</li>
             <li><?php 
					echo $this->Form->input(
											'Rents.rent_from_date', 
											array('label'=>'Rent From Date',
												'type'        => 'date'));

			?>
			</li>
		</ul>
		  <div class="btn-section">
			<div class="btns-div">
				<div class="float-rt">
            <?php 
						echo $this->Html->link(
											'Cancel', 
												array(
													'controller' =>'Rents',
													'action'=>'index',
													'id' => $apartment_id
													
											), 
												array(
													'class' =>'cancel'
												)
											);
										
											
						echo $this->Form->submit(
												'Submit', 
												array(
													'div'=>false, 
													'class' => 'btn-org-s'
												)
											);
					?></div>
			</div>
		</div>
	<?php 
		// form end
		echo $this->Form->end();
	?>

					</div>
				</div>
			</td>
		</tr>
	</table>
	</div>
</div>
