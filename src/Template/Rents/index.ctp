<div class="headline">
	<div class="left">
		<h2>
			<?php echo "Rents for ".$apartment_name;?>
		</h2>
	</div>
<div class="right">
<?= $this->Html->link('Add New Rent', ['action' => 'add','id'=>$apartment_id]); ?>                                   		
		
	</div>
 </div>
<div class="content-holder">
	<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td class="leftnav" valign="top">
				<div id="arrow"></div>
				<?php 
					echo $this->element('admin_left_menu');
				?>
			</td>
			<td class="content" valign="top" width="99%">
            <table cellpadding="0" cellspacing="1" width="98%" class="user-grid">
					<tr>
						<th>
							Rent From Date
						</th>
						<th>
							Rent Amount
						</th>
						<th>
						     Actions
						</th>
                   </tr>
				   	<?php if ($rents->isEmpty()): ?>
						<tr>
							<td colspan="2" class="center">
								<?php echo 'Rents Not Found'; ?>
							</td>
						</tr>
						<?php else: ?>						
						<?php 
							$i = 1;
							foreach ($rents as $rents) 
							{
						?>
								<tr>
									<td>
										<?php echo $rents->rent_from_date; ?>
									</td>
									<td>
										<?php echo $rents->rent_amount; ?>
									</td>
									<td>
									 <div class="icons moduleicons">
                                      <?php
                                        echo $this->Html->link(
																			'<span class="icon-edit"></span>',
																			array(
																				'controller' =>'Rents',
																				'action' => 'edit', 
																			   'rentid'=>$rents->rent_id,
																				'id'=>$apartment_id
																			),
																			array(
																				'escape' => false, 
																				'title'=>'Edit Rent Details', 
																				
																			)
																		);
									  ?>
                                    </div>
								   </td>
								</tr>
						<?php 
								$i++;
							}
						?>		
					<?php endif; ?>
				</table>
			</td>
		</tr>
	</table>
   </div>	
									