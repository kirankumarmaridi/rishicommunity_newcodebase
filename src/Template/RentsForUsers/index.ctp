<div class="headline">
	<div class="left">
		<h2>
			<?php echo "Users Rent for ".$apartment_name;?>
		</h2>
	</div>

 <div class="right">
		<?php echo $this->Html->link(
										'Add New Tenant',
										array(
											'controller' => 'RentsForUsers', 
											'action' => 'add',
											'id'=> $apartment_id
									),
										array(
											'escape' => false, 
											'title'=>'Add Tenant Rent'
										)
							);
								
							
		?>
	</div>
  </div>
<div class="content-holder">
	<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td class="leftnav" valign="top">
				<div id="arrow"></div>
				<?php 
					echo $this->element('admin_left_menu');
				?>
			</td>
			
			<td class="content" valign="top" width="99%">
            <table cellpadding="0" cellspacing="1" width="98%" class="user-grid">
					<tr>
						<th>
                        User Rent Amount
						</th>
                         <th>
						Rent Start Date
						</th>
						
                        <th>
							 ACTIONS
						</th>
                   </tr>
					<?php if ($rentusers->isEmpty()): ?>
						<tr>
							<td colspan="2" class="center">
								<?php echo "User Rents Not Found";?>
							</td>
						</tr>
						<?php else: ?>						
						<?php 
							$i = 1;
							foreach ($rentusers as $rentusers) 
							{
						?>
								<tr <?php if($i%2 == 0) echo 'class="even"';?>>
									<td>
										<?php echo $rentusers->rents_for_user_rent_amount;?>
									</td>
                                    <td> 
										<?php echo $rentusers->rents_for_user_from_date;?>
                                    </td>
									<td>
                                     <div class="icons moduleicons">
                                      <?=$this->Html->link(
																			'<span class="icon-edit"></span>',
																			array(
																				'controller' =>'RentsForUsers',
																				'action' => 'edit', 
																				'rentuserid' => $rentusers->rents_for_user_id,
																				'id' => $apartment_id
																			),
																			array(
																				'escape' => false, 
																				'title'=>'Edit User Rent Details', 
)
																		);
									  ?>
                                    </div>
								   </td>
								</tr>
						<?php 
								$i++;
							}
						?>		
					<?php endif; ?>
				</table>
			</td>
		</tr>
	</table>
</div>	