
		<div class="headline">
		<div class="left"><h2><?php echo 'User Rent';?></h2></div>
		<div class="right"></div>
	</div>
	<div class="content-holder">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="leftnav" valign="top">
					<div id="arrow"></div>
					<?php 
						echo $this->element('admin_left_menu');
					?>
				</td>
	
				<td class="content" valign="top" width="99%">
					<div class="add-div">

	<div class="pop-head"><?php echo 'Create User Rent'." for ".$apartment_name;?></div>
	<?php 
		echo $this->Form->create('RentsForUsers',
								array(
									'url' => array(
										'controller' => 'RentsForUsers', 
										'action' => 'add',
										'id' => $apartment_id
									),
									'inputDefaults' => array(
										'label' => false
									),
									'class' => 'popup-form', 
									'type'  => 'file'
								)
							);
	?>
	<?php /*?><?php 
					echo $this->Form->input(
											'apt_id', 
											array( 
												'type' => 'hidden',
												'value' => $apartment_id
											)
										);
				?><?php */?>
		<ul>
           	<li>
				
				<?php 
					echo $this->Form->input(
											'rents_for_user_rent_amount', 
											array(
											       'label'=>'User Rent'
												 )
										);	
				?>
			</li>
           </ul>
           <ul>
           	<li>
				
				<?php 
					echo $this->Form->input(
											'rents_for_user_from_date', 
											array( 
												'lable'=>'Start Date',
											    'type' => 'date'
										     )	
										);	
				?>
			</li>
           </ul>
		
			
		<div class="btn-section">
			<div class="btns-div">
				<div class="float-rt">
		<?php 
						echo $this->Html->link(
											'Cancel', 
												array(
													'action'=>'index',					
												) , 
												array(
													'class' =>'cancel'
												)
											);
											
						echo $this->Form->submit(
												'Submit', 
											array(
													'div'=>false, 
													'class' => 'btn-org-s'
												)
											);
					?></div>
			</div>
		</div>
	<?php 
		// form end
		echo $this->Form->end();
	?>


					</div>
				</div>
			</td>
		</tr>
	</table>
	</div>
	</div>