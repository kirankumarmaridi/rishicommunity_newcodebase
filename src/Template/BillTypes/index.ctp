<div class="headline">
	<div class="left">
		<h2>
			<?php echo 'BillTypes';?>
		</h2>
	</div>
<div class="right">	
<?= $this->Html->link('Add New Bill Type', ['action' => 'add']); ?>                                   					
</div>
 </div>
<div class="content-holder">
	<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td class="leftnav" valign="top">
				<div id="arrow"></div>
				<?php 
					echo $this->element('admin_left_menu');
				?>
			</td>
			<td class="content" valign="top" width="99%">
					<table cellpadding="0" cellspacing="1" width="98%" class="user-grid">
				           <tr>        
						        <th>
									Bill Type Name
						      </th>
                             <th>Actions</th>
					     </tr>
					 	<?php if ($billtypes->isEmpty()): ?>
						 <tr>
							<td colspan="2" class="center">
								<?php echo 'Bill Types Not Found';?>
							</td>
						</tr>
					    <?php else: ?>						
						<?php 
							$i = 1;
							foreach ($billtypes as $billtypes) 
							{
						?>
						<tr <?php if($i%2 == 0) echo 'class="even"';?>>
									<td>
										<?php echo $billtypes->bill_type_name;?>
									</td>
								    <td>
									 <div class="icons moduleicons">
                                   		<?php 
												echo $this->Html->link(
																			'<span class="icon-edit"></span>',
																			array(
																				'controller' =>'BillTypes',
																				'action' => 'edit', 
																				 $billtypes->bill_type_id
																			),
																			array(
																				'escape' => false, 
																				'title'=>'Edit BillType Details', 
																				
																			)
																		);
																														
												echo $this->Html->link(
																			'<span class="icon-delete"></span>', 
																			array(
																				'controller' =>'BillTypes',
																				'action' => 'delete', 
																				$billtypes->bill_type_id
																			),
																			array(
																				'escape' => false, 
																				)
																				
																		);
											?>
										</div>
									</td>
								</tr>
						<?php 
								$i++;
							}
						?>		
					<?php endif; ?>
				</table>
              </td>
		</tr>
	</table>
   </div>	
   
                                      
                                   