   <?php 
	echo $this->Html->css('jquery-ui');
	echo $this->Html->script("vendor/jquery-1.9.1.min");
	echo $this->Html->script("vendor/jquery-ui");
	echo $this->Html->script('jquery.validate');
	echo $this->Html->css('jquery.validate');	
	
?>
<script type="text/javascript">
$(function() {
    $(".datepicker").datepicker({ 
    	defaultDate: null,
    	changeMonth: true,
      	changeYear: true,
      	yearRange: '1900:' + new Date().getFullYear(),
      	dateFormat: "yy-mm-dd" 
    });    
});
</script>
	<div class="headline">
		<div class="left"><h2><?php echo 'Tenant Apartment';?></h2></div>
		<div class="right"></div>
	</div>
	<div class="content-holder">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="leftnav" valign="top">
					<div id="arrow"></div>
					<?php 
						echo $this->element('admin_left_menu');
					?>
				</td>
				<td class="content" valign="top" width="99%">
					<div class="add-div">
	<div class="popup-div">
  <div class="pop-head"></div>
	<?php 

		echo $this->Form->create($tenantapt,
		                             array(
									 'inputDefaults' => array(
										'label' => false
									),
									'class' => 'popup-form', 
									'type' => 'file'
								)
								);
								echo $this->Form->hidden('TenantAptData.tenant_apt_data_id');
	?>

		
    <ul>
			<li>
			
				<?php 
					echo $this->Form->input(
											'TenantAptData.tenant_apt_data_joining_date', 
											  array( 
												'placeholder'=>'Tenant Apartment Joining Date',
												'label'=>'Tenant Apartment Joining Date',
												  'type' => 'date',
												 'minYear' => date('Y') -0,
                                                 'maxYear' => date('Y') + 10,
											)
										);
				?>
			</li>
            <li>
                            <?php
				 echo $this->Form->input('TenantAptData.tenant_apt_data_vacating_date',
				                          array( 
												'placeholder'=>'Tenant Apartment Vacating Date',
												'label'=>'Tenant Apartment Vacating Date',
												  'type' => 'date',
												 'minYear' => date('Y') -0,
                                                 'maxYear' => date('Y') + 10,
												
											)
								   );
			   ?>
			</li>
			<li>
                
                <?php
				 echo $this->Form->input('TenantAptData.tenant_apt_data_notice_date',
				                          array( 
												'placeholder'=>'Tenant Apartment Notice Date',
												'label'=>'Tenant Apartment Notice Date',
												  'type' => 'date',
												 'minYear' => date('Y') -0,
                                                 'maxYear' => date('Y') + 10,
												
											)
								   );
			   ?>
			</li>
			<li>
			<?php
			  echo $this->Form->input('TenantAptData.tenant_apt_data_aptid',
			                           array(
									    'type' => 'select',
										'options'=>$apts,
									    'label'=>'Apartment Name'
									    )
									   );
			?>
			</li>
            <li>
			 
			<?php
			  echo $this->Form->input('TenantAptData.tenant_apt_data_state',
			                           array('type' => 'select',
									   'options'=>$tenantsnm,
									   'label'=>'Tenant State')
									   );
			?>
			</li>
         </ul>
        <div class="btn-section">
			<div class="btns-div">
				<div class="float-rt">
					<?php 
						echo $this->Html->link(
												'Cancel', 
												array(
												    'controller' =>'TenantAptData',
													'action'=>'index',
													'id'=>$tenant_id
												), 
												array(
													'class' =>'cancel'
												)
											);
						echo $this->Form->submit(
												'Submit', 
												array(
													'div'=>false, 
													'class' => 'btn-org-s'
												)
											);
					?>
				</div>
			</div>
		</div>
	<?php 
		// form end
		echo $this->Form->end();
	?>
					</div>
				</div>
			</td>
		</tr>
	</table>
	</div>
				