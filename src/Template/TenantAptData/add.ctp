   <?php 
	echo $this->Html->css('jquery-ui');
	echo $this->Html->script("vendor/jquery-1.9.1.min");
	echo $this->Html->script("vendor/jquery-ui");
	echo $this->Html->script('jquery.validate');
	echo $this->Html->css('jquery.validate');	
	
?>
<script type="text/javascript">
$(function() {
    $(".datepicker").datepicker({ 
    	defaultDate: null,
    	changeMonth: true,
      	changeYear: true,
      	yearRange: '1900:' + new Date().getFullYear(),
      	dateFormat: "yy-mm-dd" 
    });    
});
</script>
<div class="headline">
		<div class="left"><h2><?php echo 'Tenant Apartments';?></h2></div>
		<div class="right"></div>
	</div>
	<div class="content-holder">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="leftnav" valign="top">
					<div id="arrow"></div>
					<?php 
						echo $this->element('admin_left_menu');
					?>
				</td>
				<td class="content" valign="top" width="99%">
					<div class="add-div">
<div class="pop-head">
<?php echo 'Create Tenant Apartment';?></div>
	
	<?php 
		echo $this->Form->create('TenantAptData',
								array(
									'url' => array(
										'controller' =>'TenantAptData',
										'action' => 'add',
										$tenant_id
									),
									'inputDefaults' => array(
										'label' => false
									),
									'class' => 'popup-form', 
									'type' => 'file'
								)
							);
		?>
			<?php 
					echo $this->Form->input(
											'tenant_id', 
											array( 
												'type' => 'hidden',
												'value' =>$tenant_id
											)
										);
				?>
		
    <ul>
			<li>
			
				<?php 
					echo $this->Form->input(
											'tenant_apt_data_joining_date', 
											  array( 
												'placeholder'=>'Tenant Apartment Joining Date',
												'label'=>'Tenant Apartment Joining Date',
												  'type' => 'date',
												 'minYear' => date('Y') -0,
                                                 'maxYear' => date('Y') + 10,
											)
										);
				?>
			</li>
            <li>
                            <?php
				 echo $this->Form->input('tenant_apt_data_vacating_date',
				                          array( 
												'placeholder'=>'Tenant Apartment Vacating Date',
												'label'=>'Tenant Apartment Vacating Date',
												  'type' => 'date',
												 'minYear' => date('Y') -0,
                                                 'maxYear' => date('Y') + 10,
												
											)
								   );
			   ?>
			</li>
			<li>
                
                <?php
				 echo $this->Form->input('tenant_apt_data_notice_date',
				                          array( 
												'placeholder'=>'Tenant Apartment Notice Date',
												'label'=>'Tenant Apartment Notice Date',
												  'type' => 'date',
												 'minYear' => date('Y') -0,
                                                 'maxYear' => date('Y') + 10,
												
											)
								   );
			   ?>
			</li>
			<li>
			<?php
			  echo $this->Form->input('tenant_apt_data_aptid',
			                           array(
									    'type' => 'select',
										'options'=>$apts,
									    'label'=>'ApartmentName'
									    )
									   );
			?>
			</li>
            <li>
			 
			<?php
			  echo $this->Form->input('tenant_apt_data_state',
			                           array('type' => 'select',
									   'options'=>$tenantsnm,
									   'label'=>'Tenant State')
									   );
			?>
			</li>
         </ul>
		 <div class="btn-section">
			<div class="btns-div">
				<div class="float-rt">
      
					<?=$this->Html->link(
												'cancel', 
											array(
												'action'=>'index',
											   $tenant_id
												), 
												array(
													'class' =>'cancel'
												)
												);
											?>
						<?php echo $this->Form->submit(
												'Submit', 
												array(
													'div'=>false, 
													'class' => 'btn-org-s'
												)
											);
					?>
			</div>
			</div>
		</div>
	<?php 
		// form end
		echo $this->Form->end();
	?>
					</div>
				</div>
			</td>
		</tr>
	</table>
	</div>