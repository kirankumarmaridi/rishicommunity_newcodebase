<div class="headline">
	<div class="left">
		<h2>
			<?php
			 echo "Apartments for ".$tenantnm;
			 ?>
		</h2>
	</div>
<div class="right">
 <?= $this->Html->link('Add New Tenant Apartment',['action' => 'add',$tenant_id]) ?>
 </div>
 </div>
<div class="content-holder">
	<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td class="leftnav" valign="top">
				<div id="arrow"></div>
				<?php 
					echo $this->element('admin_left_menu');
				?>
			</td>
			<td class="content" valign="top" width="99%">
            <table cellpadding="0" cellspacing="1" width="98%" class="user-grid">
					<tr>
           
						<th>
				 							
								Tenant Apartment Joining Date	
						
						</th>
                        <th>
							Tenant Apartment Vacating Date
						</th>
						<th>
							Tenant Apartment Notice Date
						</th>
                        <th>
							Tenant Apartment Name
						</th>
                         <th>
							Tenant State Name
						</th>
                         <th>
					    Actions
						</th>
                   </tr>
				  	<?php if ($tenan->isEmpty()): ?>
						<tr>
							<td colspan="2" class="center">
								<?php echo 'Tenant Apartments Not Found';?>
							</td>
						</tr>
						<?php else: ?>						
						<?php 
							$i = 1;
							foreach ($tenan as $tenan) 
							{
						?>
								<tr <?php if($i%2 == 0) echo 'class="even"';?>>
									<td>
										<?php echo $tenan->tenant_apt_data_joining_date;?>
									</td>
									<td>
										<?php echo $tenan->tenant_apt_data_vacating_date;?>
									</td>
									<td>
										<?php echo $tenan->tenant_apt_data_notice_date;?>
									</td>									
										<td>
										<?php 
									
		      						    print_r((json_decode($this->requestAction("/TenantAptData/getapartmentName/".$tenan->tenant_apt_data_aptid))));
									 ?>
									 </td>
									 <td>
									 <?php 
									
		      						    print_r((json_decode($this->requestAction("/TenantAptData/getTenantStateName/".$tenan->tenant_apt_data_state))));
									 ?>
									 </td>
                                    <td>
									<div class="icons moduleicons">
                                      <?php
                                        echo $this->Html->link(
															   '<span class="icon-edit"></span>',
																			array(
																				'controller' =>'TenantAptData',
                 															'action' => 'edit',
																				 'tenantaptid'=>$tenan->tenant_apt_data_id,
																				 'id'=> $tenant_id,
																				 'tenantstid'=>$tenan->tenant_apt_data_state
                                   							
                                   							
																		),
																			array(
																				'escape' => false, 
																				'title'=>'Edit Tenant Apartments Details'
																			)
																		);
									  ?>
                                    </div>
								   </td>
						</tr>
						<?php 
								$i++;
							}
						?>		
					<?php endif; ?>
				</table>
			</td>
		</tr>
	</table>
   </div>	