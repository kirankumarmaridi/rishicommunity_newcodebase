<div class="headline">
	<div class="left">
		<h2>
			<?php echo 'TenantStates';?>
		</h2>
	</div>
    <div class="right">
	<?= $this->Html->link('Add Tenant State', ['action' => 'add']); ?> </div>
 </div>
<div class="content-holder">
	<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td class="leftnav" valign="top">
				<div id="arrow"></div>
				<?php 
					echo $this->element('admin_left_menu');
				?>
			</td>
			<td class="content" valign="top" width="99%">
				<table cellpadding="0" cellspacing="1" width="98%" class="user-grid">
			
				      <tr>
				    	<th>
							Tenant State Id
						</th>
						<th>
							
							Tenant Name
						</th>
                        <th>Actions</th>
					</tr>
					<?php if ($tenantStates->isEmpty()): ?>
						<tr>
							<td colspan="3" class="center">
								<?php echo 'Tenant State Not Found';?>
							</td>
						</tr>
					<?php else: ?>						
						<?php 
							$i = 1;
							foreach ($tenantStates as $tenantState) 
							{
						?>
                      <tr <?php if($i%2 == 0) echo 'class="even"';?>>
                        	        <td>
										<?php echo $tenantState->tenant_state_id;?>
									</td>
									<td>
									<?php echo $tenantState->tenant_state_name;?>
									<td>
									  <div class="icons moduleicons">
                                   		<?php 
												echo $this->Html->link(
																			'<span class="icon-edit"></span>',
																			array(
																				'controller' =>'TenantStates',
																				'action' => 'edit', 
																				$tenantState->tenant_state_id
																			),
																			array(
																				'escape' => false, 
																				'title'=>'Edit Tenant States Details',
																				
																			)
																		);
																														
												echo $this->Html->link(
																			'<span class="icon-delete"></span>', 
																			array(
																				'controller' =>'TenantStates',
																				'action' => 'delete', 
																				$tenantState->tenant_state_id
																			),
																			array(
																				'escape' => false, 
																				'title'=>'Delete Tenant States Details',
																				
																			)
																		);
																		
																	
											?>
										</div>
									</td>
								</tr>
						<?php 
								$i++;
							}
						?>		
					<?php endif; ?>
				</table>
              </td>
		</tr>
	</table>
   </div>	
   