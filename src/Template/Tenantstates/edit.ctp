<div class="headline">
		<div class="left"><h2><?php echo 'Tenant States';?></h2></div>
		<div class="right"></div>
	</div>
	<div class="content-holder">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="leftnav" valign="top">
					<div id="arrow"></div>
					<?php 
						echo $this->element('admin_left_menu');
					?>
				</td>
				<td class="content" valign="top" width="99%">
					<div class="add-div">
			
	<div class="popup-div">
    <div class="pop-head"><?php echo 'Edit Tenant State Details';?></div>
	<?php 
		          echo $this->Form->create($tenantStates,
				  array(
				  'inputDefaults' => array(
										'label' => false
									),
									'class' => 'popup-form', 
									'type' => 'file'
								)	
								);		
								echo $this->Form->hidden('Tenantstate.tenant_state_id');		
		      ?>
    <ul>
			<li>
				
				<?php 
					echo $this->Form->input(
											'Tenantstates.tenant_state_name', 
											array('label'=>'Tenant State Name'));
				?>
			</li>
          </ul>
		   <div class="btn-section">
			<div class="btns-div">
				<div class="float-rt">
					<?php 
						echo $this->Html->link(
											'Cancel', 
												array(
													'action'=>'index'
												), 
												array(
													'class' =>'cancel'
												)
											);
											
						echo $this->Form->submit(
												'Submit', 
												array(
													'div'=>false, 
													'class' => 'btn-org-s'
												)
											);
					?></div>
			</div>
		</div>
	<?php 
		// form end
		echo $this->Form->end();
	?>

					</div>
				</div>
			</td>
		</tr>
	</table>
	</div>
	</div>
