
	<div class="headline">
		<div class="left"><h2><?php echo 'Edit Contact';?></h2></div>
		<div class="right"></div>
	</div>
	<div class="content-holder">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td class="leftnav" valign="top">
					<div id="arrow"></div>
					<?php 
						echo $this->element('admin_left_menu');
					?>
				</td>
				
				<td class="content" valign="top" width="99%">
					<div class="add-div">

	<?php 
		echo $this->Form->create($contact,
								array(
								      'url' => array(
										'controller' =>'Settings', 
										'action' => 'editcontact',
										'id'=>1
									),
									'inputDefaults' => array(
										'label' => false
									),
									'class' => 'popup-form', 
									'type' => 'file'
								)
							);
?>

<ul>
			<li>
				
				<?php 
				
					echo $this->Form->input(
											'contact_address', 
											array( 
												'placeholder'=> "Address",
												'default' => $contact->contact_address,
												'rows' => '1', 
												'div' => true, 
												'cols' => '5'
											)
										);
				?>
			</li>
			<li>
				
				<?php 
					echo $this->Form->input(
											'contact_email', 
											array( 
												'placeholder'=> "Email",
												'default' => $contact->contact_email
											)
										);
				?>
			</li>
			<li>
				
				<?php 
					echo $this->Form->input(
											'contact_phone', 
											array( 
												'placeholder'=> "Phone",
												'default' => $contact->contact_phone
											)
										);
				?>
			</li>
			<li>
			
				<?php 
					echo $this->Form->input(
											'contact_website', 
											array( 
												'placeholder'=> "Website",
												'default' => $contact->contact_website
											)
										);
				?>
			</li>
         </ul>

		<div class="btn-section">
			<div class="btns-div">
				<div class="float-rt">
					<?php 
						echo $this->Form->submit(
												'SUBMIT', 
												array(
													'div'=>false, 
													'class' => 'btn-org-s'
												)
											);
					?>
				</div>
			</div>
		</div>
	<?php 
		// form end
		echo $this->Form->end();
	?>

					</div>
				</div>
			</td>
		</tr>
	</table>
	</div>