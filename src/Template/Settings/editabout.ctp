
	<div class="headline">
		<div class="left"><h2><?php echo 'Edit About';?></h2></div>
		<div class="right"></div>
	</div>
	<div class="content-holder">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="leftnav" valign="top">
					<div id="arrow"></div>
					<?php 
						echo $this->element('admin_left_menu');
					?>
				</td>
				<td class="content" valign="top" width="99%">
					<div class="add-div">
	<?php 
	
		echo $this->Form->create($about,
								array(
								      'url' => array(
										'controller' =>'Settings', 
										'action' => 'editabout',
										'id'=>1
									),
									'inputDefaults' => array(
										'label' => false
									),
									'class' => 'popup-form', 
									'type' => 'file'
								)
							);
?>
    <ul>
			<li>
			
				<?php 
				 
					echo $this->Form->input(
											'about_data', 
											array( 
											    'label'=>'About Us',
												'placeholder'=> 'About Us',
												'rows' => '2', 
												'div' => true, 
												'cols' => '5'
											)
										);
				?>
			</li>
         </ul>
         <div class="btn-section">
			<div class="btns-div">
				<div class="float-rt">
					<?php 
						echo $this->Form->submit(
												'Submit', 
												array(
													'div'=>false, 
													'class' => 'btn-org-s'
												)
											);
					?>
				</div>
			</div>
		</div>
	<?php 
		// form end
		echo $this->Form->end();
	?>

					</div>
				</div>
			</td>
		</tr>
	</table>
	</div>