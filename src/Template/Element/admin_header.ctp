<div class="header_section">
	<div class="logo_sec">
		<?php 
			echo $this->Html->link(
							'Rishi Community', 
							array(
								'controller' => 'Admins',
								'action' => 'login'
							)
						);
        ?>
	</div>
	<div class="right">		
		<?php 
			if($this->request->session())
			{
				echo $this->element('admin_login_menu');
			}
		?>
	</div>
</div>