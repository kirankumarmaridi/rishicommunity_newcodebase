<!--  
/**
 * Created on August 30, 2012
 * elements/footer
 *
 * This is the footer for all layouts
 *
 * @author Vineela Yarlagadda <vineela.yarlagadda@valuelabs.net>
 *
 * */	
-->
<div class="paging">
	<?php if ($this->Paginator->hasPrev()): ?>
		<?php 
			echo $this->Paginator->prev(
										__d('api_generator', 'Prev'), 
										array(), 
										null, 
										array(
											'class'=>'disabled'
										)
									);
		?>
	<?php endif; ?>
	
	<?php if ($this->Paginator->hasPage(null, 2)): ?>
 		<?php echo $this->Paginator->numbers(); ?>
	<?php endif; ?>

	<?php if ($this->Paginator->hasNext()): ?>
		<?php 
			echo $this->Paginator->next(
										__d('api_generator', 'Next'), 
										array(), 
										null, 
										array(
											'class'=>'disabled'
										)
									);
		?>
	<?php endif;?>
</div>
