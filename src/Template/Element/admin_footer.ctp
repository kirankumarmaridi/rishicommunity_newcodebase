<div class="footer_section">
	<div class="left">
		<p>
			<?php echo 'CopyRight';?> &copy; <?php echo 'CopyRight_Year';?>. <?php echo 'All Rights Reserved';?>.
		</p>
	</div>
	<div class="right">
		<ul>
			<li>
				<a href="javascript:void(0)" title="<?php echo 'Privacy Policy';?>">
					<?php echo 'Privacy Policy';?>
				</a>
			</li>
			<li>
				<a href="javascript:void(0)" title="<?php echo 'Terms Of use';?>">
					<?php echo 'Terms Of use';?>
				</a>
			</li>
		</ul>
	</div>
</div>