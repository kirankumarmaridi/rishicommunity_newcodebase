<!--  
/**
 * Created on June 25, 2015
 * elements/footer
 *
 * This is the header for all layouts
 *
 * */	
-->
<header class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <div class="nav-collapse collapse pull-right">
                    <ul class="nav">
                        <li class="active">
                           	<?php 
                        		echo $this->Html->link(
									'Home', 
									[
										'controller' => 'home',
										'action' => 'index'
									]
								);
                        	?>
                        </li>
                        <li>
                        	<?php 
                        		echo $this->Html->link(
									'About Us', 
									[
										'controller' => 'home',
										'action' => 'about'
									]
								);
                        	?>
						</li>
                        
                        <li>
                        	<?php 
                        		echo $this->Html->link(
									'FAQ', 
									[
										'controller' => 'home',
										'action' => 'faq'
									]
								);
                        	?>
                        </li>
                        <li>
                        	<?php 
                        		echo $this->Html->link(
									'Contact', 
									[
										'controller' => 'home',
										'action' => 'contact'
									]
								);
                        	?>
                        </li>
                    </ul>        
                </div><!--/.nav-collapse -->
            </div>
        </div>
    </header>
    <!-- /header -->

    <!--Slider-->
    <section id="slide-show">
     <div id="slider" class="sl-slider-wrapper">

        <!--Slider Items-->    
        <div class="sl-slider">
            <!--Slider Item1-->
            <div class="sl-slide item1" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
                <div class="sl-slide-inner">
                    <div class="container">
                        <h2>Rishi Community</h2>
                    </div>
                </div>
            </div>
            <!--/Slider Item1-->

            <!--Slider Item2-->
            <div class="sl-slide item2" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">
                <div class="sl-slide-inner">
                    <div class="container">
                        <h2>Rishi Community</h2>
                    </div>
                </div>
            </div>
            <!--Slider Item2-->

            <!--Slider Item3-->
            <div class="sl-slide item3" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">
                <div class="sl-slide-inner">
                   <div class="container">
                        <h2>Rishi Community</h2>
                </div>
            </div>
        </div>
        <!--Slider Item3-->

    </div>
    <!--/Slider Items-->

</div>
<!-- /slider-wrapper -->           
</section>
<!--/Slider-->