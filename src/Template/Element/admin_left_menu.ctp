<div id="leftnavd-div">

	<ul id="menu">
		<li>
			<?php echo $this->Html->link(
										'My Dashboard', 
			   							array( 
											'controller' => 'Admins', 
											'action' => 'account' 
										)
									); 
			?>	
		</li>
		<li>
			<?php echo $this->Html->link(
										'Tenants', 
			   							array( 
											'controller' =>'Tenants', 
											'action' => 'index' 
										)
									); 
			?>	
		</li><li>
			<?php echo $this->Html->link(
										'Apartments', 
			   							array( 
											'controller' =>'Apartments', 
											'action' => 'index' 
										)
									); 
			?>	
		</li>
		<li>
			<?php echo $this->Html->link(
										'Tenantstates', 
			   							array( 
											'controller' =>'Tenantstates', 
											'action' => 'index' 
										)
									); 
			?>	
		</li>
		<li>
			<?php echo $this->Html->link(
										'BillTypes', 
			   							array( 
											'controller' =>'BillTypes', 
											'action' => 'index' 
										)
									); 
			?>	
		</li>
		<li>
			<?php echo $this->Html->link(
										'DiscountCoupons', 
			   							array( 
											'controller' =>'DiscountCoupons', 
											'action' => 'index' 
										)
									); 
			?>	
		</li>
		<li>
			<?php echo $this->Html->link(
										'AccomodationType', 
			   							array( 
											'controller' =>'AccomodationTypes', 
											'action' => 'index' 
										)
									); 
			?>	
		</li>
		<li>
			<?php echo $this->Html->link(
										'StaffUsers', 
			   							array( 
											'controller' =>'StaffUsers', 
											'action' => 'index' 
										)
									); 
			?>	
		</li>
		<li>
			<?php echo $this->Html->link(
										'AboutUs', 
			   							array( 
											'controller' =>'Settings', 
											'action' => 'editabout',
											'id' => 1
										),
										array( 
											'id'=>'Settings'.'_editabout', 
											'title' =>'About Us'
										)
									); 
			?>	
		</li>
		<li>
			<?php echo $this->Html->link(
										'Contact', 
			   							array( 
											'controller' =>'Settings', 
											'action' => 'editcontact',
											'id' => 1
										),
										array( 
											'id'=>'Settings'.'_editcontact', 
											'title' =>'Contact'
						
										)
									); 
			?>	
		</li>
		<li>
			<?php echo $this->Html->link(
										'Myprofile', 
			   							array( 
											'controller' =>'Admins', 
											'action' => 'changeprofile' 
										)
									); 
			?>	
		</li>
		<li>
			<?php echo $this->Html->link(
										'ChangePassword', 
			   							array( 
											'controller' =>'Admins', 
											'action' => 'changepassword' 
										)
									); 
			?>	
		</li>
		
		
	</ul>

</div>
