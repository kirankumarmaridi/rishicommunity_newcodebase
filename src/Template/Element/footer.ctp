<!--  
/**
 * Created on June 25, 2015
 * elements/footer
 *
 * This is the footer for all layouts
 *
 * */	
-->


<!--Bottom-->
<section id="bottom" class="main">
    <!--Container-->
    <div class="container">

        <!--row-fluids-->
        <div class="row-fluid">

            <!--Contact Form-->
            <div class="span4">
                <h4>ADDRESS</h4>
                <ul class="unstyled address">
                    <li>
                        <i class="icon-envelope"></i>
                        <strong>Email: </strong> <?php echo $contacts->contact_email;?>
                    </li>
                    <li>
                        <i class="icon-globe"></i>
                        <strong>Website:</strong> <?php echo $contacts->contact_website;?>
                    </li>
                    <li>
                        <i class="icon-phone"></i>
                        <strong>Toll Free:</strong> <?php echo $contacts->contact_phone;?>
                    </li>
                </ul>
            </div>
            <!--End Contact Form-->
            
            <div class="span4">
                <h4>OUR COMPANY</h4>
                <div>
                    <ul class="arrow">
                        <li>
                        	<?php 
                        		echo $this->Html->link(
									'About Us', 
									array(
										'controller' => 'home',
										'action' => 'about'
									)
								);
                        	?>
                        </li>
                        <li>
                        	<?php 
                        		echo $this->Html->link(
									'Terms of Use', 
									array(
										'controller' => 'home',
										'action' => 'tou'
									)
								);
                        	?>
                        </li>
                        <li>
                        	<?php 
                        		echo $this->Html->link(
									'Privacy Policy', 
									array(
										'controller' => 'home',
										'action' => 'pp'
									)
								);
                        	?>
                        </li>
                        <li>
							<?php 
                        		echo $this->Html->link(
									'We are hiring', 
									array(
										'controller' => 'home',
										'action' => 'career'
									)
								);
                        	?>
                        </li>
                    </ul>
                </div>  
            </div>
            <!--Important Links-->

    </div>
    <!--/row-fluid-->
</div>
<!--/container-->

</section>
<!--/bottom-->

<!--Footer-->
<footer id="footer">
    <div class="container">
        <div class="row-fluid">
            <div class="span5 cp">
                &copy; 2015 <a target="_blank" href="http://rishicommunity.com/" title="Rishi Community">RishiCommunity</a>. All Rights Reserved.
            </div>
            <!--/Copyright-->

            <div class="span6">
            </div>

            <div class="span1">
                <a id="gototop" class="gototop pull-right" href="#"><i class="icon-angle-up"></i></a>
            </div>
            <!--/Goto Top-->
        </div>
    </div>
</footer>
<!--/Footer-->

<?php 
	echo $this->Html->script("vendor/jquery-1.9.1.min");
	echo $this->Html->script("vendor/bootstrap.min");
	echo $this->Html->script("jquery.ba-cond.min");
	echo $this->Html->script("jquery.slitslider");
?>

<!-- SL Slider -->
<script type="text/javascript"> 
$(function() {
    var Page = (function() {

        var $navArrows = $( '#nav-arrows' ),
        slitslider = $( '#slider' ).slitslider( {
            autoplay : true
        } ),

        init = function() {
            initEvents();
        },
        initEvents = function() {
            $navArrows.children( ':last' ).on( 'click', function() {
                slitslider.next();
                return false;
            });

            $navArrows.children( ':first' ).on( 'click', function() {
                slitslider.previous();
                return false;
            });
        };

        return { init : init };

    })();

    Page.init();
});
</script>
<!-- /SL Slider -->