<section class="no-margin">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d806193.069685007!2d-122.42033749999999!3d37.87919985!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x808583a3a688d7b5%3A0x8c891b8457461fa9!2sSan+Francisco+Bay+Area%2C+CA!5e0!3m2!1sen!2sus!4v1435275118293" width="1170" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>
    <section id="contact-page" class="container" style="padding-top:20px">
        <div class="row-fluid">

            <div class="span8">
                <h4>Contact Form</h4>
                <div class="status alert alert-success" style="display: none"></div>

                <form id="main-contact-form" class="contact-form" name="contact-form" method="post" action="contact">
                  <div class="row-fluid">
                    <div class="span5">
                        <label>First Name</label>
                        <input type="text" name="firstname" class="input-block-level" required="required" placeholder="Your First Name">
                        <label>Last Name</label>
                        <input type="text" name="lastname" class="input-block-level" required="required" placeholder="Your Last Name">
                        <label>Email Address</label>
                        <input type="text" name="email" class="input-block-level" required="required" placeholder="Your email address">
                    </div>
                    <div class="span7">
                        <label>Message</label>
                        <textarea name="message" id="message" required="required" class="input-block-level" rows="8"></textarea>
                    </div>

                </div>
                <button type="submit" class="btn btn-primary btn-large pull-right">Send Message</button>
                <p> </p>

            </form>
        </div>
        
        <div class="span3">
            <h4>Our Address</h4>
			<?php 
							$i = 1;
							foreach ($contacts as $contacts) 
							{
						?>
            <table>
            <tr>
                <td>
                   <p>
                      <i class="icon-map-marker"></i>&nbsp;&nbsp;
				      <?php echo $contacts->contact_address;?>
                   </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                       <i class="icon-envelope"></i>&nbsp;&nbsp;
					   <?php echo $contacts->contact_email;?>
                   </p>
                </td>
            </tr>
            <tr>
                <td>
                   <p>
                      <i class="icon-phone"></i>&nbsp;&nbsp;
					  <?php echo $contacts->contact_phone;?>
                   </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                       <i class="icon-globe"></i>&nbsp;&nbsp;
					   <?php echo $contacts->contact_website;?>
                   </p>
                </td>
            </tr> 
			<?php 
								$i++;
							}
						?>		
            </table>
        </div>
    </div>
</section>
<?php 
	echo $this->Html->script("vendor/jquery-1.9.1.min");
?>