<!-- viadmin forgotpassword form start -->
<div id="login">
	<div class="forgot_box">
		<div class="forgot_area">
			<div class="forgotpassword-head"></div>
			<?php 
				// form creation
				echo $this->Form->create('Admins',
										array(
											'controller' => 'admins', 
											'action' => 'forgotpassword',
											'inputDefaults' => array( 
												'label' => false
											)
										)
									);
			?>
			<ul>
				<li>
					<?php 
						// viadmin email text field for forgotpassword
						echo $this->Form->input(
												'email', 
												array(
													'placeholder'=>"Email Id", 
													'type'=>'email', 
													'class' => 'text-box'
												)
											);						
						// login link
						echo $this->Html->link(
												"Login".'?', 
												array(
													'action'=>'login'
												), 
												array( 
													'class' => 'links pushtop-2 float-rt pushbottom-3'
												)
											);
						?>
					</li>
					<li class="center">
						<?php 
							// submit button
							echo "<p>".$this->Form->submit(
															"Forgot Password", 
															array(
																'div'=>false, 
																'class' => 'btn-org-forgot', 
																'id' =>'submit_butt'
															)
														);
						?>
					</li>
				</ul>	
			<?php 
				// form end
				echo $this->Form->end();
			?>
		</div>
	</div>
</div>
<!-- viadmin forgotpassword form end -->