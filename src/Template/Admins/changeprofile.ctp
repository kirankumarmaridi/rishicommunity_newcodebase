<div class="headline">
	<div class="left"><h2><?php echo 'Change Profile';?></h2></div>
	<div class="right"></div>
</div>
<!-- viadmin changeprofile heading end -->

<!-- viadmin changeprofile form start -->
<div class="content-holder">
	<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td class="leftnav" valign="top">
				<div id="arrow"></div>
				<?php 
					echo $this->element('admin_left_menu');
				?>
			</td>
			<td class="content" valign="top" width="99%">
				<div class="add-div">
					<div class="pop-head"><?php echo 'Change Profile';?></div>
					<?php 
						// form creation
						echo $this->Form->create($c_prof,
									array(
										'url' => array(
												    				'controller' => 'admins', 
												    				'action' => 'changeprofile'
																),
										'inputDefaults' => array( 
											'label' => false
										),
									'class' => 'popup-form', 
									)
								);
					?>
					<div class="mandatory-fields"> * <?php echo 'Mandatory Fields'; ?></div>
						<ul>
							<li>
									<?php
									// viadmin firstname text field
									echo $this->Form->input(
															'Admins.admin_email', 
															array(
																'label' => 'EmailId',
																'placeholder' => 'EmailId'
															)
														);
								?>
							</li>
							<li>
								<?php
									// viadmin firstname text field
									echo $this->Form->input(
															'Admins.admin_firstname', 
															array(
																'label' => 'FirstName',
																'placeholder' => 'Firstname'
															)
														);
								?>
							</li>
							<li>
								<?php 
									// viadmin lastname text field
									echo $this->Form->input(
															'Admins.admin_lastname', 
															array(
																'label' => 'LastName',
																'placeholder' => 'LastName'
															)
														);
								?>
							</li>
							<li>
								<?php 
									// viadmin mobile number text field
									echo $this->Form->input(
															'Admins.admin_mobile', 
															array(
																'label' => 'Mobile No',
																'placeholder' => 'Mobile No'
															)
														);
								?>
							</li>
						</ul>

						<div class="btn-section">
							<div class="btns-div">
								<div class="float-rt">										
									<?php
										// cancel link like button
										echo $this->Html->link(
																'Cancel', 
																array(
																	'action'=>'account'
																),
																array(
																	'class' =>'cancel'
																)
															);
										// submit button
										echo $this->Form->submit(
																'Update profile', 
																array(
																	'div'=>false, 
																	'class' => 'btn-org-s'
																)
															);	
									?>							
								</div>
							</div>
						</div>
					<?php 
						// form end
						echo $this->Form->end();
					?>
				</div>
			</td>
		</tr>
	</table>					
</div>