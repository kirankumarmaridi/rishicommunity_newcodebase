
<div class="headline">
	<div class="left"><h2><?php echo 'Chanage Password';?></h2></div>
	<div class="right"></div>
</div>
<!-- viadmin changepassword heading end -->

<!-- viadmin changepassword form start -->
<div class="content-holder">
	<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td class="leftnav" valign="top">
				<div id="arrow"></div>
				<?php 
					// including admin left menu
					echo $this->element('admin_left_menu');
				?>
			</td>
			
			<td class="content" valign="top" width="99%">	
				<div class="add-div">
					<div class="pop-head"><?php echo 'Change Password';?></div>
					<?php 
						// form creation
						echo $this->Form->create('Admins',
									array(
										'url' => array(
												    				'controller' => 'admins', 
												    				'action' => 'changepassword'
																),
										'inputDefaults' => array( 
											'label' => false
										),
									'class' => 'popup-form', 
									)
								);
					?>
					<div class="mandatory-fields"><?php echo 'Mandatory Fields'; ?></div>
						<ul>
							<li>
								<?php
									// password text field
									echo $this->Form->input(
															'admin_password', 
															array(
																'type' => 'password', 
																'label' => 'Newpassword',
																'placeholder'=>'NewPassword'
															)
														);
								?>
							</li>
							<li>
								<?php 
									// confirm password text field
									echo $this->Form->input(
															'admin_confirm_password', 
															array(
																'type' => 'password',
																'label' => 'ConfirmPassword',
																'placeholder'=>'ConfirmPassword'
															)
														);
								?>
							</li>
						</ul>

						<div class="btn-section">
							<div class="btns-div">
								<div class="float-rt">
									<?php
										// cancel button
										echo $this->Html->link(
																'Cancel', 
																array(
																	'action'=>'account'
																),
																array(
																	'class' =>'cancel'
																)
															);
										// submit button
										echo $this->Form->submit(
																'Change Password', 
																array(
																	'div'=>false, 
																	'class' => 'btn-org-s'
																)
															);	
									?>
								</div>
							</div>
						</div>
					<?php 
						// form end
						echo $this->Form->end();
					?>
				</div>			
			</td>
		</tr>
	</table>					
</div>