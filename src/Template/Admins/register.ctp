<div class="content-holder">
	<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td class="leftnav" valign="top">
				<div id="arrow"></div>
				<div id="leftnavd-div"></div>
			</td>
			<td class="content" valign="top" width="99%">
				<div class="add-div">
					<div class="pop-head"><?php echo "Register";?></div>
					<?php 
						// form creation
						echo $this->Form->create('Admins', 
												array(
													'class' => 'popup-form',
													'inputDefaults' => array( 
														'label' => false
													),
												    'url' => array(
												    				'controller' => 'admins', 
												    				'action' => 'register'
																)
												)
											);
					?>
					<div class="mandatory-fields"> * <?php echo 'Mandatory Fields'; ?></div>
						<ul>
						
							<li>
								<?php 
									// viadmin email text field
									echo $this->Form->input(
															'admin_email', 
															array( 
																'label' => 'EmailId',
																'placeholder' => 'EmailId', 
																'type'=>'email'
															)
														);
								?>
							</li>
							<li>
								<?php 
									// viadmin firstname text field
									echo $this->Form->input(
															'admin_firstname', 
															array( 
																'label' => 'FirstName',
																'placeholder' => 'FirstName'
															)
														);
								?>
							</li>
							<li>
								<?php 
									// viadmin lastname text field
									echo $this->Form->input(
															'admin_lastname', 
															array(
																'label' => 'LastName',
																'placeholder' => 'LastName'
															)
														);
							?>
							</li>
							<li>
								<?php 
									// viadmin lastname text field
									echo $this->Form->input(
															'admin_mobile', 
															array(
																'label' => 'MobileNo',
																'placeholder' => 'MobileNo'
															)
														);
							?>
							</li>
							<li>
								<?php 
									// viadmin mobile text field
									echo $this->Form->input(
															'admin_password', 
															array(
																'label' => 'Password',
																'placeholder' => 'Password'
															)
														);
							?>
							</li>
						</ul>
						<div class="btn-section">
							<div class="btns-div">
								<div class="float-rt">
									<?php
										// cancel  button
										echo $this->Html->link(
																	'Cancel', 
																	array(
																		'action'=>'viadmins'
																	), 
																	array(
																		'class' =>'cancel'
																	)
																);
									?>
									<?php 
										// submit button
										echo $this->Form->submit(
																'Create Account', 
																array(
																	'div'=>false, 
																	'class' => 'btn-org-s'
																)
															);
									?>
								</div>
							</div>
						</div>
					<?php 
						// form end
						echo $this->Form->end();
					?>

				</div>
			</td>
		</tr>
	</table>
</div>