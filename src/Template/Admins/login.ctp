<!-- login form start -->
<div class="login_box">
	<div class="login_area">
		<div class="login-head"></div>		  
			<?php echo $this->Form->create('Admins',
									array(
										'url' => array(
												    				'controller' => 'admins', 
												    				'action' => 'login'
																)
										)
									);?>
		<ul>
			<li>
				<?php 
					// viadmin email text field
					echo $this->Form->input(
											'admin_email', 
											array(
												'placeholder'=>"Email ID", 
												'class' => 'text-box'
											)
										);
				?>
			</li>
			<li>
				<?php 
					// viadmin password text field
					echo $this->Form->input(
											'admin_password', 
											array(
												'placeholder'=>"Password", 
												'type' => 'password', 
												'class' => 'text-box'
											)
										);
						
					// cancel button like link
					echo $this->Html->link(
											"Forgot Password".'?', 
											array(
												'action'=>'forgotpassword'
											), 
											array( 
												'title' => "Forgot Password", 
												'class' => 'links pushtop-2 float-rt pushbottom-3'
											)
										);
				?>
			</li>
			<li class="center">
				<?php 
					// submit button
					echo "<p>".$this->Form->submit(
													"Login", 
													array(
														'div'=>false, 
														'class' => 'btn-org', 
														'title' => "Login", 
														'id' => 'submit_butt'
													)
												);
				?>
			</li>
		</ul>	
		<?php 
			// form end
			echo $this->Form->end();
		?>
	</div>
</div>
<!-- login form end -->