<div class="headline">
	<div class="left">
		<h2>
			<?php echo 'DiscounCoupons';?>
		</h2>
	</div>
<div class="right">
	<?= $this->Html->link('Add Discount Coupon', ['action' => 'add']) ?>
				</div>
 </div>
<div class="content-holder">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
			<td class="leftnav" valign="top">
				<div id="arrow"></div>
				<?php 
					echo $this->element('admin_left_menu');
				?>
			</td>
			<td class="content" valign="top" width="99%">
				<table cellpadding="0" cellspacing="1" width="98%" class="user-grid">
					<tr>
						<th>
							
															Discount Coupon Code
						</th>
                        <th>
				            Discount Coupon Description
   						</th>
						<th>
							
															Discount Coupon Price
						</th>
                        <th>
							
				Discount Coupon Amount Type												
						</th>
                        <th>
							
								ACTIONS
							
						</th>
					</tr>
						<?php if ($discountcoupons->isEmpty()): ?>
											<tr>
							<td colspan="7" class="center">
								<?php echo 'DiscountCoupons Not Found';?>
							</td>
						</tr>
					<?php else: ?>						
						<?php 
							$i = 1;
							foreach ($discountcoupons as $discountcoupons) 
							{
						?>
								<tr <?php if($i%2 == 0) echo 'class="even"';?>>
									<td>
										<?php echo $discountcoupons->discount_coupon_code;?>
									</td>
									<td>
										<?php echo $discountcoupons->discount_coupon_desc;?>
									</td>
									<td>
										<?php echo $discountcoupons->discount_coupon_price;?>
									</td>
                                    <td>
										<?php echo $discountcoupons->discount_coupon_amount_type;?>
									</td>
                                       <td>
                                 <div class="icons moduleicons">
                                   		<?php 
												echo $this->Html->link(
																			'<span class="icon-edit"></span>',
																			array(
																				'controller' =>'DiscountCoupons',
																				'action' => 'edit', 
																				$discountcoupons->discount_coupon_id,
																			),
																			array(
																				'escape' => false, 
																				'title'=>'Edit DiscountCoupons'
																			)
																		);
																														
												echo $this->Html->link(
																			'<span class="icon-delete"></span>', 
																			array(
																				'controller' =>'DiscountCoupons',
																				'action' => 'delete', 
																				$discountcoupons->discount_coupon_id,
																			),
																			array(
																				'escape' => false, 
																				'title'=>'Delete DiscountCoupons'
																			)
																			
																		);
											?>
										</div>
									</td>
								</tr>
						<?php 
								$i++;
							}
						?>		
					<?php endif; ?>
				</table>
              </td>
		</tr>
	</table>
   </div>	
   