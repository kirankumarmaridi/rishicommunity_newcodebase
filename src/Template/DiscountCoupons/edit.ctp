	<div class="left"><h2><?php echo 'Discouncoupons';?></h2></div>
		<div class="right"></div>
	</div>
	<div class="content-holder">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="leftnav" valign="top">
					<div id="arrow"></div>
					<?php 
						echo $this->element('admin_left_menu');
					?>
				</td>
				<td class="content" valign="top" width="99%">
					<div class="add-div">
	<div class="popup-div">


    <div class="pop-head"><?php echo 'Edit DiscountCoupon Details';?></div>
	<?php 
		echo $this->Form->create($discountcoupons,
		                             array(
									'inputDefaults' => array(
										'label' => false
									),
									'class' => 'popup-form', 
									'type' => 'file'
								)
							);
echo $this->Form->hidden('DiscountCoupons.discount_coupon_id');
	?>
    <ul>
			<li>
				
				<?php 
					echo $this->Form->input(
											'DiscountCoupons.discount_coupon_code', 
											array( 
												'placeholder'=>'Discount Coupon Code',
												'label'=>'Discount Coupon Code'
											)
										);
				?>
			</li>
            <li>
           
                <?php
				 echo $this->Form->input(
				                         'DiscountCoupons.discount_coupon_desc',
				                          array( 
												'placeholder'=>'Discount Coupon description',
												'label'=>'Discount description'
											)
								   );
			   ?>
			</li>
			<li>
			
				<?php 
					echo $this->Form->input(
											'DiscountCoupons.discount_coupon_price', 
											array( 
												'placeholder'=>'Discount Coupon price',
												'label'=>'Discount price',
												'type' => 'number',
												)
										);	
				?>
			</li>
            <li>
				
				<?php 
				      $options = array(
                                       'Dollars' => 'Dollars',
									   'Percentage' => 'Percentage',
								  );
                      echo $this->Form->input('DiscountCoupons.discount_coupon_amount_type',
					                                    array('options' => $options)
									    );
			   ?>
             </li>	
           </ul>
		         <div class="btn-section">
			<div class="btns-div">
				<div class="float-rt">
       
					<?php 
						echo $this->Html->link(
												'Cancel', 
												array(
													'action'=>'index'
												), 
												array(
													'class' =>'cancel'
												)
											);
						echo $this->Form->submit(
												'Submit', 
												array(
													'div'=>false, 
													'class' => 'btn-org-s'
												)
											);
					?>
			
	</div>
			</div>
		</div>
	<?php 
		// form end
		echo $this->Form->end();
	?>
					</div>
				</div>
			</td>
		</tr>
	</table>
	</div>
	</div>
