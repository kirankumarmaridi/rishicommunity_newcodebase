<div class="headline">
		<div class="left"><h2><?php echo 'AccomodationType';?></h2></div>
		<div class="right"></div>
	</div>
	<div class="content-holder">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="leftnav" valign="top">
					<div id="arrow"></div>
					<?php 
						echo $this->element('admin_left_menu');
					?>
				</td>
		<td class="content" valign="top" width="99%">
					<div class="add-div">
	
	<div class="popup-div">
<div class="pop-head"><?php echo 'Create Accomodation Type';?></div>
<?php
		echo $this->Form->create('AccomodationType',
								array(
									'url' => array(
										'controller' =>'AccomodationTypes', 
										'action' => 'add'
									),
									'inputDefaults' => array(
										'label' => false
									),
									'class' => 'popup-form', 
									'type' => 'file'
								)
							);
	?>
    <ul>
			<li>
				
				<?php 
					echo $this->Form->input(
					                     'accomodation_type_name',
										 array('placeholder'=>'Accomodation Type Name',
	                                                  'type'=>'text',
													     'label'=>'AccomodationType Name')
										);
				?>
			</li>
    </ul>
	 <div class="btn-section">
			<div class="btns-div">
				<div class="float-rt">
					<?php 
						echo $this->Html->link(
												'Cancel', 
												array(
													'action'=>'index'
												), 
												array(
													'class' =>'cancel'
												)
											);
											
						echo $this->Form->submit(
												'Submit', 
												array(
													'div'=>false, 
													'class' => 'btn-org-s'
												)
											);
					?></div>
			</div>
		</div>
	<?php 
		// form end
		echo $this->Form->end();
	?>
	</div>
				</div>
			</td>
		</tr>
	</table>
	</div>
	</div>
