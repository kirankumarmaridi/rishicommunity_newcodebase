	<div class="headline">
	<div class="left">
		<h2>
			<?php echo 'AccomodationType';?>
		</h2>
	</div>
<div class="right">
<?= $this->Html->link('Add New Accomodation Type', ['action' => 'add']) ?>
</div>
 </div>
<div class="content-holder">
	<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td class="leftnav" valign="top">
				<div id="arrow"></div>
				<?php 
					echo $this->element('admin_left_menu');
				?>
			</td>
			<td class="content" valign="top" width="99%">

				<table cellpadding="0" cellspacing="1" width="100%" class="user-grid">
					<tr>
					<th>
									Accomodation Type Id
						</th>
						 <th>
									Accomodation Type Name
						</th>
                        <th>
						Actions
						</th>
					</tr>
						<?php if ($accomodation->isEmpty()): ?>
						<tr>
							<td colspan="2" class="center">
								<?php echo 'Accomadation Type Not Found';?>
							</td>
						</tr>
					<?php else: ?>						
						<?php 
							$i = 1;
							foreach ($accomodation as $accomodation) 
							{
						?>
							<tr <?php if($i%2 == 0) echo 'class="even"';?>>
								<td>
										<?php echo $accomodation->accomodation_type_id;?>
									</td>
									<td>
										<?php echo $accomodation->accomodation_type_name;?>
									</td>
							
								 <td>
								 <div class="icons moduleicons">
                                   		<?php 
												echo $this->Html->link(
																			'<span class="icon-edit"></span>',
																			array(
																				'controller' =>'AccomodationTypes',
																				'action' => 'edit', 
																				$accomodation->accomodation_type_id
																			),
																			array(
																				'escape' => false, 
																				'title'=>'Edit Accomodation Details', 
																				)
																		);?>
																		<?php 
												echo $this->Html->link(
																			'<span class="icon-delete"></span>',
																			array(
																				'controller' =>'AccomodationTypes',
																				'action' => 'delete', 
																				$accomodation->accomodation_type_id
																			),
																			array(
																				'escape' => false, 
																				'title'=>'Delete Accomodation Details', 
																				)
																		);?>
                                  
                                  
                                 </div>
									</td>
								</tr>
						<?php 
								$i++;
							}
						?>		
					<?php endif; ?>
				</table>
                <?php /*?><?php 
					if(!empty($apartments))
					{
						echo $this->element('pagination');
					}
				?><?php */?>	
			</td>
		</tr>
	</table>
   </div>	
   