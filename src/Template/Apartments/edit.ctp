<div class="headline">
		<div class="left"><h2><?php echo 'Apartments';?></h2></div>
		<div class="right"></div>
	</div>
	<div class="content-holder">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="leftnav" valign="top">
					<div id="arrow"></div>
					<?php 
						echo $this->element('admin_left_menu');
					?>
				</td>
				<td class="content" valign="top" width="99%">
					<div class="add-div">
	<div class="popup-div">


    <div class="pop-head"><?php echo 'Edit Apartments details';?></div>
	<?php 		echo $this->Form->create($apartments,
	                                       array(
										   'inputDefaults' => array(
										'label' => false
									),
									'class' => 'popup-form', 
									'type' => 'file'
								)
							);
							echo $this->Form->hidden('Apartments.apt_id');
							
	?>
    <ul>
			<li>
				
				<?php 
					echo $this->Form->input(
											'Apartments.apt_name', 
											array( 
												
												'label'=>'Apartment Name'
											)
										);
				?>
			</li>
            <li>
                
                <?php
				 echo $this->Form->input(
				                         'Apartments.apt_address',
				                          array( 
												'placeholder'=>'Address',
												'label'=>'Address'
											)
								   );
			   ?>
			</li>
			<li>
				
				<?php 
					echo $this->Form->input(
											'Apartments.apt_bedrooms', 
											array( 
												'placeholder'=>'Bedrooms',
												'label'=>'Bedrooms',
												'type' => 'number',
												'min' => '1',
												'max' => '6'
											)
										);	
				?>
			</li>
            <li>
				
				<?php 
					echo $this->Form->input(
											'Apartments.apt_total_occupancy', 
											array( 
												'placeholder'=>'TOTAL OCCUPANCY',
												'label'=>'Total Occupancy',
												'type' => 'number',
												'min' => '1',
												'max' => '20'
											)
										);	
				?>
			</li>
            <li>
           
				<?php 
					echo $this->Form->input(
											'Apartments.apt_bathrooms', 
											array( 
												'placeholder'=>'BATHROOMS',
												'label'=>'Bath Rooms',
												'type' => 'number',
												'min' => '1',
												'max' => '4'
											)
										);	
				?>
              </li>
               <li>
             
				<?php 
					echo $this->Form->input(
											'Apartments.apt_half_bathrooms', 
											array( 
												'placeholder'=> 'HALF BATHROOMS',
												'label'=>'Half Bath Rooms',
												'type' => 'number',
												'min' => '1',
												'max' => '4'
											)
										);	
				?>
              </li>
              <li>
              
				<?php 
					echo $this->Form->input(
											'Apartments.apt_car_parkings', 
											array( 
												'placeholder'=>'CAR PARKING',
												'label'=>'Car Parking',
												'type' => 'number',
												'min' => '0',
												'max' => '4'
											)
										);	
				?>
              </li>
              <li>
            
				<?php 
					echo $this->Form->input(
											'Apartments.apt_bedroom1_occupancy', 
											array( 
												'placeholder'=>'BEDROOM1 OCCUPANCY',
												'label'=>'BedRoom1 Ocuupancy',
												'type' => 'number',
												'min' => '1',
												'max' => '3'
											)
										);	
				?>
              </li>
              <li>
             
				<?php 
					echo $this->Form->input(
											'Apartments.apt_bedroom2_occupancy', 
											array( 
												'placeholder'=>'BEDROOM2 OCCUPANCY',
												'label'=>'BedRoom2 Ocuupancy',
												'type' => 'number',
												'min' => '0',
												'max' => '3'
											)
										);	
				?>
              </li>
              <li>
               
				<?php 
					echo $this->Form->input(
											'Apartments.apt_bedroom3_occupancy', 
											array( 
												'placeholder'=>'BEDROOM3 OCCUPANCY',
											'label'=>'BedRoom3 Ocuupancy',
												'type' => 'number',
												'min' => '0',
												'max' => '6'
											)
										);	
				?>
              </li>
              <li>
          
				<?php 
					echo $this->Form->input(
											'Apartments.apt_bedroom4_occupancy', 
											array( 
												'placeholder'=>'BEDROOM4 OCCUPANCY',
                                                 'label'=>'BedRoom4 Ocuupancy',
												'type' => 'number',
												'min' => '0',
												'max' => '3'
											)
										);	
				?>
              </li>
              <li>
              
				<?php 
					echo $this->Form->input(
											'Apartments.apt_living_room_occupancy', 
											array( 
												'placeholder'=>'LIVING ROOM OCCUPANCY',
												'label'=>'Living Room Ocuupancy',
												'type' => 'number',
												'min' => '0',
												'max' => '3'
											)
										);	
				?>
              </li>
               <li>

				<?php 
					echo $this->Form->input(
											'Apartments.apt_persons_required_on_lease', 
											array( 
												'placeholder'=> 'NUMBER OF PERSONS REQUIRED ON LEASE',
												'label'=>'Number Of Persons Required On Lease',
												'type' => 'number',
												'min' => '0',
												'max' => '3'
											)
										);	
				?>
              </li>
               <li>
                
				<?php 
					echo $this->Form->input(
											'Apartments.apt_persons_on_current_lease', 
											array( 
												'placeholder'=> 'NUMBER OF PERSONS ON CURRENT LEASE',
												'label'=>'Number Of Persons On Current Lease',
												'type' => 'number',
												'min' => '0',
												'max' => '3'
											)
										);	
				?>
              </li>
               <li>
                
				<?php 
					echo $this->Form->input(
											'Apartments.apt_marketing_email', 
											array( 
												'placeholder'=> 'MARKETING EMAIL',	
												'label'=>'Marketing Email',
											)
										);	
				?>
              </li>
         </ul>
		 <div class="btn-section">
			<div class="btns-div">
				<div class="float-rt">
					<?php 
						echo $this->Html->link(
												'Cancel', 
												array(
													'action'=>'index')
											, 
												array(
													'class' =>'cancel'
												)
											);
						echo $this->Form->submit(
												'Submit', 
												array(
													'div'=>false, 
													'class' => 'btn-org-s'
												)
											);
					?>
				</div>
			</div>
		</div>
	<?php 
		// form end
		echo $this->Form->end();
	?>
					</div>
				</div>
			</td>
		</tr>
	</table>
	</div>
	</div>