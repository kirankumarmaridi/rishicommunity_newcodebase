		<div class="headline">
	<div class="left">

		<h2>
			<?php echo 'APARTMENTS';?>
		</h2>
		</div>
<div class="right">
<?= $this->Html->link('Add Apartment', ['action' => 'add']) ?>
</div>
 </div>
<div class="content-holder">
<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td class="leftnav" valign="top">
				<div id="arrow"></div>
				<?php 
					echo $this->element('admin_left_menu');
				?>
			</td>
			<td class="content" valign="top" width="99%">
				<table cellpadding="0" cellspacing="1" width="98%" class="user-grid">
					<tr>
						<th>
							
															Apartment Name
						</th>
                        <th>
				            Address
   						</th>
						<th>
							
															Bedrooms
						</th>
                        <th>
							
															Total Occupancy
						</th>
						<th>
						Lease End Date
						</th>
						<th>
						Current Rent
						</th>
                        <th>
							
								ACTIONS
							
						</th>
					</tr>
						<?php if ($apartments->isEmpty()): ?>
											<tr>
							<td colspan="7" class="center">
								<?php echo 'Apartments Not Found';?>
							</td>
						</tr>
					<?php else: ?>						
						<?php 
							$i = 1;
							foreach ($apartments as $apartment) 
							{
						?>
								<tr <?php if($i%2 == 0) echo 'class="even"';?>>
									<td>
										<?php echo $apartment->apt_name;?>
									</td>
									<td>
										<?php echo $apartment->apt_address;?>
									</td>
									<td>
										<?php echo $apartment->apt_bedrooms;?>
									</td>
                                    <td>
										<?php echo $apartment->apt_total_occupancy;?>
									</td>
									<td>
									 <?php 
									
		      						    print_r((json_decode($this->requestAction("/Apartments/getCurrentLeaseEndDateOfApartment/".$apartment->apt_id))));
									 ?>
									</td>
									<td>
									 <?php 
									
		      						    print_r((json_decode($this->requestAction("/Apartments/getCurrentRentDateOfApartment/".$apartment->apt_id))));
									?>
									</td>
                                       <td>
                                 <div class="icons moduleicons">
                                   		<?php 
												echo $this->Html->link(
																			'Leases',
																			array(
																				'controller' =>'Leasings',
																				'action' => 'index', 
																				 'id'=>$apartment->apt_id
																			)
																		);?>
																		<br>
																		<?php
												echo $this->Html->link(
																			'Rents',
																			array(
																				'controller' =>'Rents',
																				'action' => 'index', 
																				  'apid'=>$apartment->apt_id
																			)
																		);
																		?>
																		<br>
																		<?php
												echo $this->Html->link(
																			'RentsforUsers',
																			array(
																				'controller' =>'RentsForUsers',
																				'action' => 'index', 
																				'id'=>$apartment->apt_id
																			)
																		);
																
																?>
																<br>
																<?php
												echo $this->Html->link(
																			'View Apartments',
																			array(
																				'controller' =>'Apartments',
																				'action' => 'view', 
																				'id'=>$apartment->apt_id
																			)
																		);
																
																?>
																<br>
																<?php
												echo $this->Html->link(
																			'<span class="icon-edit"></span>',	
																			[																																					
																				'action' => 'edit', 
																				$apartment->apt_id
																			],
																			[
																				'escape' => false
																				]
																		);
												echo $this->Html->link(
																			'<span class="icon-delete"></span>',	
																			[																																					
																				'action' => 'delete', 
																				$apartment->apt_id
																			],
																			[
																				'escape' => false
																				]
																		);
																		
																		?>
										</div>
									</td>
								</tr>
						<?php 
								$i++;
							}
						?>		
					<?php endif; ?>
				</table>
                <?php /*?><?php 
					if(!empty($apartments))
					{
						echo $this->element('pagination');
					}
				?><?php */?>	
			</td>
		</tr>
	</table>
   </div>	
   