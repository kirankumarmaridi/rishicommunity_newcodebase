		 <div class="headline">
	<div class="left">
		<h2>
			<?php echo "Apartment details of ".$apartment->apt_name;?>
		</h2>
	</div>
    <div class="right">
	</div>
	</div>
<div class="content-holder">
	<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td class="leftnav" valign="top">
				<div id="arrow"></div>
				<?php 
					echo $this->element('admin_left_menu');
				?>
			</td>
			
               <td class="content" valign="top" width="81%">
				<table cellpadding="0" cellspacing="1" width="100%" class="user-grid">
                     <tr>
                       <td width="50%">
						  <?php 							
							echo "Apartment Name";
						  ?>
                        </td>
                        <td>
						  <?php echo $apartment->apt_name;?>
					   </td>
                    </tr>
                      <tr>
                        <td>
                          <?php 							
						     echo "Address";
							?>
						</td>
						<td>
								<?php echo $apartment->apt_address;?>
						</td>
					 </tr>
                     <tr>
                        <td>               
				          <?php 							
						   echo "Bedrooms";
					     ?>
                    </td>
					<td>
					   <?php echo $apartment->apt_bedrooms;?>
					</td>
					</tr>
                    <tr>      
                      <td>   
			            <?php 	
					      echo "Total Occupancy";			
					    ?>
			        </td>
                    <td>
				      <?php echo $apartment->apt_total_occupancy;?>
				    </td>
                    </tr>
                    <tr>
                      <td>
                        <?php 							
						  echo "Bathrooms";
					    ?>
                      </td>
				       <td>
						  <?php echo $apartment->apt_bathrooms;?>
					  </td>
					  </tr>
					  <tr>
                        <td>
                			<?php 							
								echo "Half Bathrooms";
							?>
                        </td>
						<td>
							<?php echo $apartment->apt_half_bathrooms;?>
					    </td>
						</tr>
                        <tr>
                         <td>
							<?php 							
								echo "Car Parking";
							?>
                         </td>
						<td>
							  <?php echo $apartment->apt_car_parkings;?>
						</td>
						</tr>
                        <tr>
                         <td>
							<?php 							
								echo "Bedroom1 Occupancy";
							?>
                         </td>
						<td>
							  <?php echo $apartment->apt_bedroom1_occupancy;?>
						</td>
						</tr>
                          <td>
                        	<?php 							
								echo "Bedroom2 Occupancy";
							?>
						  </td>
							<td>
							    <?php echo $apartment->apt_bedroom2_occupancy;?>
							</td>
							</tr>
                         <tr>
                          <td>
               				<?php 							
								echo "Bedroom3 Occupancy";
							?>
                        </td>
						<td>
										<?php echo $apartment->apt_bedroom3_occupancy;?>
							</td>
							</tr>
                       </tr>
                          <td>
                        
							<?php 							
								echo "Bedroom4 Occupancy";
							?>
						
                        </td>
							<td>
										<?php echo $apartment->apt_bedroom4_occupancy;?>
							</td>
							</tr>
                        </tr>
                          <td>
							<?php 							
								echo "Livingroom Occupancy";
							?>
			             </td>
						<td>
						      <?php echo $apartment->apt_living_room_occupancy;?>
						</td>
							</tr>
                        </tr>
				         <tr>
                        <td>
							<?php 	
							      echo "Number of Persons Required on Lease";
							?>
					    
                        <td>
                             <?php echo $apartment->apt_persons_required_on_lease;?>
                        </td>
                        </tr>
                        <tr>
                          <td>
							<?php 	
							      echo "Number of Persons on current Lease";
							?>
					     <td>
                             <?php echo $apartment->apt_persons_on_current_lease;?>
                        </td>
                        </tr>
                        <tr>
                        <td>
                          <?php 	
							      echo "Marketing Email";
							?>
					    <td>
                             <?php echo $apartment->apt_marketing_email;?>
                        </td>
                        </tr>
                     	</tr>
                        </tr> 	
				</table>	
			</td>
		</tr>
	</table>
   </div>	