<?php
// s/Model/Table/PostsTable.php

namespace App\Model\Table;

use Cake\ORM\Table;

class TenantAptDataTable extends Table
{
  public function initialize(array $config)
    {
        $this->table('Tenant_Apt_Data');
		
        $this->displayField('tenant_apt_data_state');
		 $this->displayField('apt_name');
    
/*'foreignKey' => 'tenant_id',
'joinType' => 'LEFT',*/
		//[
//		'className'=>'TenantsTable',
//		'foreignkey'=>'tenant_id',
//		'bindingkey'=>'tenant_apt_data_tenantid']);
//parent::initialize();
//$this->loadComponent('Auth');
    }
	     public function getTenantApartmentId($id) {
	 	$query= $this->find('all',
	    							             array(
	    								               'fields'     => 'tenant_apt_data_id',
	    								               'conditions' => array(
													                         'tenant_apt_data_tenantid' => $id
																       )
												
	    							             )
	   );
	    $row = $query->first();
		return $row->tenant_apt_data_id;		
	    }	
			  				  
}
?>
	