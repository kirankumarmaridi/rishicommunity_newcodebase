<?php
// s/Model/Table/PostsTable.php

namespace App\Model\Table;

use Cake\ORM\Table;


class LeasingsTable extends Table
{
  public function initialize(array $config)
    {
         $this->Table('Leasings');
	
    }
	 function getCurrentLeaseEndDateOfApartment($apartment_id)
    { 
         $query= $this->find('all',
                                 array( 
                                         'conditions' => array(
                                                        'lease_apartment_id'=>$apartment_id,
                                                        'Leasings.lease_end_date >=' => date('y-m-d'),
                                                        'Leasings.lease_start_date <=' => date('y-m-d')),
                                         'order' =>array( 
                                                        'Leasings.lease_end_date' => 'ASC'),
		                        		'fields'	=> array(
		                        						'lease_end_date'
		                        		),
                                         'limit'=>1    
                                      )
                           );
						   $lease=$query->first();
    	if (!empty($lease)) {									 
			return $lease->lease_end_date;
		} else {
			return "No Current Lease available";
		}
	}
}
?>
	