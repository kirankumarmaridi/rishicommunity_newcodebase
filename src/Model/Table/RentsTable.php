<?php
// s/Model/Table/PostsTable.php

namespace App\Model\Table;

use Cake\ORM\Table;

class RentsTable extends Table
{
  public function initialize(array $config)
    {
      
		$this->Table('rents');
	}
	 function getCurrentRentDateOfApartment($apartment_id)
	{
		$query= $this->find('all',
		                        array(
								       'conditions'=>array(
									                      'rent_apartment_id'=> $apartment_id,
														   'Rents.rent_from_date <=' =>date('y-m-d')),
				                       'order'     =>array(
									                      'Rents.rent_from_date'=>'DESC'),
		                        		'fields'	=> array(
		                        						'rent_amount'
		                        		),
									   'limit'     => 1
									  )
									 );
			$rent=$query->first();
		if (!empty($rent)) {									 
			return $rent->rent_amount;
		} else {
			return "No Current Rent available";
		}
	}
	 
}
?>
	