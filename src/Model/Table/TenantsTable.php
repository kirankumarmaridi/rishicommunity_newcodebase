<?php
// s/Model/Table/PostsTable.php

namespace App\Model\Table;

use cake\ORM\TablRegistry;

use Cake\ORM\Table;

class TenantsTable extends Table
{

  public function initialize(array $config)
    {
        $this->table('tenants');
		$this->belongsTo('TenantApartments');
		
    }
	   public function getTenantFirstName($id) {
	 	$query= $this->find('all',
	    							             array(
	    								               'fields'     => 'tenant_firstname',
	    								               'conditions' => array(
													                         'tenant_id' =>$id
																       )
	    							             )
	    						     );
	     $row = $query->first();
		return $row->tenant_firstname;		
	 }								  								  
}
?>
	