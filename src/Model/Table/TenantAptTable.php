<?php 
namespace App\Model\Table;

use Cake\ORM\Table;

class TenantAptTable extends Table
{         
		 public function initialize(array $config)
      {
        $this->table('tenant_apt_data');
      }
	   
	   public function getTenantApartmentId($id) {
	 	$data= $this->find('all',
	    							             array(
	    								               'field'     => 'tenant_apt_data_aptid',
	    								               'conditions' => array(
													                         'tenant_apt_data_tenantid' => $id
																       ),
												
	    							             )
	                                          );
		return $data->tenant_apt_data_aptid;
	    }
	 }							  
	 ?>  