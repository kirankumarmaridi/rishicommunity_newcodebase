<?php
// s/Model/Table/PostsTable.php

namespace App\Model\Table;

use Cake\ORM\Table;

class AccomodationTypesTable extends Table
{
  public function initialize(array $config)
    {
        $this->table('accomodation_types');
    }
}
?>
	