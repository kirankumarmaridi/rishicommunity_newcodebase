<?php

namespace App\Model\Table;

use Cake\ORM\Table;

use Cake\Validation\Validator;
$validator = new Validator();

    class ContactsTable extends Table {
	 
	 public $useTable='contacts';
	
	   var $validate = array(
                             'contact_address' => array(
                                                           'contact_address_must_not_be_blank' => array(
                                                                                                           'rule'    => 'notEmpty',
                                                                                                           'message' => 'Please Enter Contact Address!'
                                                                                                     ),
													 ),
						     'contact_email'   => array(
                                                           'contact_email_must_not_be_blank' => array(
                                                                                                           'rule'    => 'email',
                                                                                                           'message' => 'Please Check your email address!'
                                                                                                 )
												     ),
							 'contact_phone'   => array(
                                                           'contact_phone_must_not_be_blank' => array(
                                                                                                           'rule'    => array('noEmpty'),
                                                                                                  )
													 ),
							 'contact_website' => array(
                                                           'contact_website_must_not_be_blank' => array(
                                                                                                           'rule'    => 'notEmpty',
                                                                                                           'message' => 'Please Check URL!'
                                                                                                 )
													)
				      );
	   function isValidUSPhoneFormat($phone){
        $phone_no=$phone['contact_phone'];
        $errors = array();
          if(empty($phone_no)) {
            $errors [] = "Please enter Phone Number";
          }
         else if (!preg_match('/^[+]{0,1}[1]{1}[-\s.][(]{0,1}[0-9]{3}[)]{0,1}[-\s.]{0,1}[0-9]{3}[-\s.]{0,1}[0-9]{4}$/', $phone_no)) {
           $errors [] = "Please enter valid Phone Number (+1-XXX-XXX-XXXX)";
         } 
         if (!empty($errors))
          return implode("\n", $errors);
         return true;
       }
   } 
?>