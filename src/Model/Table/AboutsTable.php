<?php 

namespace App\Model\Table;

use Cake\ORM\Table;

use Cake\Validation\Validator;



    class AboutsTable extends Table {
 
  
		 public function initialize(array $config)
      {
        $this->table('abouts');
		}
	
	public function getAboutId() {
		$query= $this->find('all',
	    							             array(
	    								               'fields'     => 'about_id'
	    							             )
	    						     );
	    $row = $query->first();
		return $row->about_id;	
	}
}
?>