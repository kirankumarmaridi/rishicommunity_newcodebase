<?php

namespace App\Model\Table;

use Cake\ORM\Table;

use Cake\ORM\Query;

class ApartmentsTable extends Table
{  
       
		 public function initialize(array $config)
      {
        $this->table('apartments');
		$this->belongsTo('Leasings');
		$this->belongsTo('RentsForUsers');
      }
   public function getApartmentName($id) {
	     
	 	$query= $this->find('all',
	    							             array(
	    								               'fields'     => 'apt_name',
	    								               'conditions' => array(
													                         'apt_id' => $id
																       )		   
																	   
	    							             )
	   					     );
	
          $row=$query->first();
		  return $row->apt_name;
		}
    } 
?>
