<?php
// s/Model/Table/PostsTable.php

namespace App\Model\Table;

use Cake\ORM\Table;

class TenantstatesTable extends Table
{
  public function initialize(array $config)
    {
        $this->table('tenant_states');
		
    }
	public function getTenantStateName($tenant_state_id){
	 	$data= $this->find('all',
	    							             array(
	    								               'fields'     => 'tenant_state_name',
	    								               'conditions' => array(
													                         'tenant_state_id' =>$tenant_state_id
																       )
													)
	                                  );
		 $row = $data->first();
		return $row->tenant_state_name;		
	    }
}
?>
	