<?php
namespace App\Controller;

use Cake\Event\Event;

use App\Controller\AppController;

class RentsForUsersController extends AppController
{
public function beforeFilter(Event $event) {
		
	  	 
		parent::beforeFilter($event); 
		$this->loginAction = array(
										'controller' => 'admins',
										'action' => 'login',
										'plugin' => null
									);
									 $this->viewBuilder()->layout("admin"); 
	 }
public function index()
	{
		if(!empty($this->request->query('id'))) {
		$this->loadModel("Apartments");
			$apartment_name = $this->Apartments->getApartmentName($this->request->query('id'));
			$rentusers=$this->RentsForUsers->find('all')
			                      /*  ->select()
			                        ->select('Apartments.apt_name')*/
			                        ->join([
			                                 [
					                          'table'=>'apartments',
						                      'alias'=>'Apartments',
						                      'type'=>'LEFT',
						                      'conditions'=>([
						                                       'Apartments.apt_id= RentsForUsers.rents_for_user_rent_apartment_id'
								                             ])
								             ]
								          ])
										  ->where([
											           [
														(['rents_for_user_rent_apartment_id' => $this->request->query('id')])
														]
												]);
		   $this->set('rentusers',$rentusers);		  
			$this->set('apartment_name', $apartment_name);
			/*$this->set('leases', $query);*/
			$this->set('apartment_id', $this->request->query('id'));
			$this->set('breadcrumb','Users Rent'." for ".$apartment_name );
		} else {
			$this->redirect(array('controller' => 'apartments', 'action'=>'index'));
		}
	}

	public function add()
	  {
	  $this->loadModel("Apartments");
			$apartment_name = $this->Apartments->getApartmentName($this->request->query('id'));
	       $rentusers = $this->RentsForUsers->newEntity();
		   $rentusers->rents_for_user_rent_apartment_id= $this->request->query('id');
			     if ($this->request->is('post')) {
                  $rentusers= $this->RentsForUsers->patchEntity($rentusers, $this->request->data);
			     if($this->RentsForUsers->save($rentusers)){
			    	$this->Flash->success(__('Your detail has been saved.'));
					return $this->redirect(array('action'=>'index'),null, true);
				} else {
			    	 $this->redirect(array('action'=>'index'));
				}
		  }
		  $this->set('apartment_name', $apartment_name);
			/*$this->set('leases', $query);*/
			$this->set('apartment_id', $this->request->query('id'));
		}  
		   public function edit()
	  { 
	    $this->loadModel("Apartments");
		$this->loadModel('RentsForUsers');
			$apartment_name = $this->Apartments->getApartmentName($this->request->query('id'));
    			$rents=$this->RentsForUsers->get($this->request->query('rentuserid'));
				 if ($this->request->is(['patch', 'post', 'put'])) {
                     $rents= $this->RentsForUsers->patchEntity($rents, $this->request->data);
			     if($this->RentsForUsers->save($rents)){
			    	$this->Flash->success(__('Your detail has been saved.'));
					return $this->redirect(array('action'=>'index'),null, true);
				} else {
			    	 $this->redirect(array('action'=>'index'));
				}
				$this->set('breadcrumb', 'Edit Rent User Details');
		  }
		 $this->set('apartment_name', $apartment_name);
			/*$this->set('leases', $query);*/
			$this->set('apartment_id', $this->request->query('id'));
		   $this->set('rents',$rents);
		   
	}	  		
}
?>