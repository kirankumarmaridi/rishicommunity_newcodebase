<?php
namespace App\Controller;

class HomeController extends AppController {
	
	
	public $uses = array('Contacts', 'Abouts');
	public function index() {
		
	}
	

	  public function about()
	  {
		  $this->loadModel('Abouts');
		  $query=$this->Abouts->find('all');
		  $about=$query->toArray();
			   $this->set('about',$about);
	  }

	public function faq() {

	}

	public function contact() {
	    $this->loadModel('Contacts');
		$query=$this->Contacts->find('all', 
														array(
															'conditions' => array(
																				'contact_status' => 1 
																			)
														)
												);
				
		       $contacts=$query->toArray();
			   $this->set('contacts',$contacts);
		if(!empty($this->data)) {
			if($this->sendemail($this->data)) {
			   $this->Flash->success(__('This message is for form 1.','default'));			 
			} else {
			    $this->Flash->success(__('This message is for form 1.','default'));
			}   
		}
	}
		
	
	public function tou() {

	}

	public function pp() {

	}
	
	
	public function sendemail($data) {
		
		App::uses('CakeEmail', 'Network/Email');
		$email_to = 'rishi.community@gmail.com';
		
		$name = @trim(stripslashes($data['firstname'])." ".stripslashes($data['lastname'])); 
	    $from = @trim(stripslashes($data['email'])); 
	    $subject = @trim(stripslashes($data['subject'])); 
	    $message = @trim(stripslashes($data['message'])); 

		$email    = new CakeEmail();
		
		$email->template('contact','default');
        $email->emailFormat('html');
        $email->to($email_to);
		$email->from($from);
        $email->subject($subject);
		$email->viewVars(
						array(
							'name' => $name, 
							'email' => $from,
							'message' => $message
						)
					);
    	try 
		{
			return $email->send('Smtp');
		} catch(Exception $e) {
			return 'error';
		}		
	}
	
}


?>