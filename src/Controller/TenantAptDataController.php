<?php
  
  
  namespace App\Controller;
  
  use cake\ORM\query;
  
  use Cake\Event\Event;

  
  use Cake\Datasource\ConnectionManager;
  
    class TenantAptDataController extends AppController {
	 
	/* TenantApartmentsController::loadModel('Tenants');
	 
	 $this->Apartments = TableRegistry::get('Apartments');
	 $this->Tenantstates = TableRegistry::get('Tenantstates');*/
	 public $uses='TenantAptData';
	 public function beforeFilter(Event $event) {
		
	  	 
		parent::beforeFilter($event); 
		$this->loginAction = array(
										'controller' => 'admins',
										'action' => 'login',
										'plugin' => null
									);
									 $this->viewBuilder()->layout("admin"); 
	 }public function index($id)
	{	
	if(!empty($id)) {
		   $this->loadModel('Tenants');
		
			$tenant_name = $this->Tenants->getTenantFirstName($id);
			
		$tenantapartments = $this->TenantAptData->find('all')
			                     /* ->select('Tenants.tenant_firstname')
								
                                ->join([
                                        'table' => 'Tenants',
									    'alias'=>'tenants',	
                                        'type' => 'LEFT',
                                       'conditions' => 'tenant_id = tenant_apt_data_tenantid'
									   ])
							     ->where(['tenant_apt_data_tenantid' =>$this->params['pass']['id']]);
										*/
																					/*->select(				'Tenants.tenant_firstname'
														)*/
										  
											->join([
											                 [
																'table' => 'tenants', 
																'alias' => 'Tenants',
																'type' => 'LEFT',									
																'conditions' => ([
																	'Tenants.tenant_id = TenantAptData.tenant_apt_data_tenantid'
																])
															 ]
														])
											->where([
											           [
														(['tenant_apt_data_tenantid' => $id])
														]
														]);
			$this->set('tenantnm',$tenant_name);
			$this->set('tenan', $tenantapartments);
			$this->set('tenant_id',$id);
     		} else {
			$this->redirect(array('controller' => 'tenants', 'action'=>'index'));
		}
	  }	
	      public function getapartmentName($id)
			{
			$this->loadModel('Apartments');
			 $this->response->body(json_encode($this->Apartments->getApartmentName($id)));
	         return $this->response;
			 }/*
	     	$query= $this->Apartments->find('all',
	    							             array(
	    								               'fields'     => 'apt_name',
	    								               'conditions' => array(
													                         'apt_id' => $id
																       )		   
																	   
	    							             )
	   					     );
	
   $row=$query->first();
	  
//print_r($query);
 $this->response->body(json_encode($row->apt_name));
 //print_r($this->response);
           return $this->response;*/
			 
     public function getTenantStateName($tenant_state_id)
	{
	   $this->loadModel('Tenantstates');
	  $this->response->body(json_encode($this->Tenantstates->getTenantStateName($tenant_state_id)));
	     return $this->response;
	 }
	      
      public function add($id)
	  {     
	   $this->loadModel('Apartments');
	  $apts=$this->Apartments->find('list',[
		                          'valueField' => 'apt_name']);
		                      
			
		$this->set('apts',$apts);
	   $this->loadModel('Tenantstates');
	
		$tenantsnm=$this->Tenantstates->find('list',[
		                          'valueField' => 'tenant_state_name']);
		$this->set('tenantsnm',$tenantsnm);
	  if(!empty($id)){
		  $tenantapt=$this->TenantAptData->newEntity();
		  $tenantapt->tenant_apt_data_tenantid= $id;	
		    if ($this->request->is('post')) {
                   $tenantapt= $this->TenantAptData->patchEntity( $tenantapt, $this->request->data);
			   $this->TenantAptData->tenant_apt_data_tenantid= $id;
			   if($this->TenantAptData->save( $tenantapt)){
			    	$this->Flash->success(__('Your detail has been saved.'));
					return $this->redirect(array('action'=>'index',$tenantapt->tenant_apt_data_tenantid),null, true);
				} else {
			    	 $this->redirect(array('action'=>'index',$tenantapt->tenant_apt_data_tenantid));
				}
			}
	  $this->loadModel('Tenants');
	    $this->set('tenant_name',$this->Tenants->getTenantFirstName($id));
	   $this->set('tenant_id',$id);
	    }
	  else {
			$this->redirect(array('controller' => 'tenantaptdata', 'action'=>'index'));
       }
    }
	  public function edit(){
		       
			   $this->loadModel('Apartments');
	  $apts=$this->Apartments->find('list',[
		                          'valueField' => 'apt_name']);
		                      
			
		$this->set('apts',$apts);
	   $this->loadModel('Tenantstates');
	
		$tenantsnm=$this->Tenantstates->find('list',[
		                          'valueField' => 'tenant_state_name']);
		$this->set('tenantsnm',$tenantsnm);
			/*$tenant_state_name=$this->RcTenantState->getTenantStateName($this->params['named']['tenantstid']);	*/
			$this->loadModel('Tenants');
		 	$tenant_name = $this->Tenants->getTenantFirstName($this->request->query('id'));
			/*$this->set('tenant_state_name',$tenant_state_name);*/
			$this->set('tenant_name',$tenant_name);
		  	$this->set('tenant_id', $this->request->query('id'));
			$tenantapt=$this->TenantAptData->get($this->request->query('tenantaptid'));
				 if ($this->request->is(['patch', 'post', 'put'])) {
                    $tenantapt= $this->TenantAptData->patchEntity($tenantapt, $this->request->data);
					 if($this->TenantAptData->save($tenantapt)){
			    	$this->Flash->success(__('Your detail has been updated.'));
					return $this->redirect(['action'=>'index']);
				} 
				$this->Flash->error(__('Unable to update your post.'));
				}   
		   $this->set('tenantapt', $tenantapt);  
    }
	public function view($id)
	{ 
	
	 $tenant_id=$id;
	 $this->loadModel('Tenants');
	 $details=$this->Tenants->find('all',
                                       array(
						                    'conditions' => array(
										                         'tenant_id' => $tenant_id
														       )
											)
									);
							
	 $this->set('details',$details);
	 $qu=$this->TenantAptData->find('all',
	                                             array(
	    								               'fields'     => 'tenant_apt_data_state',
	    								               'conditions' => array(
													                         'tenant_apt_data_tenantid' => $tenant_id
																       )
													 )
											);
	  $tenantapts=$qu->first();
	 $this->set('tenantapts',$tenantapts);
	   $tenant_name=$this->Tenants->getTenantFirstName($id);
	   $tenant_apt_id=$this->TenantAptData->getTenantApartmentId($id);
	   $this->set('tenant_apt_id',$tenant_apt_id);
	 $this->set('tenant_id',$tenant_id);
	  $this->set('tenant_name',$tenant_name);
	 $this->set('breadcrumb', "View Details of ".$tenant_name);						   
	}
	public function states()
	  { 
	  
	  	  $tenant_id=$this->request->query('id');
		$tenant_apt_id=$this->request->query('tenant_apt_id');
		  $this->loadModel('Tenantstates');
     	   $query=$this->Tenantstates->find('list',[
		                          'valueField' => 'tenant_state_name']);
		                      
			$this->set('states',$query);	
			//$this->loadModel('TenantAptData');
			 $this->set('tenant_state_name',$this->request->query('tenant_state'));			
			if($this->request->is('post')){
			  $tst=$this->TenantAptData->updateAll( array( 
				                           'tenant_apt_data_state'=> $this->request->data['TenantAptData']['tenant_apt_data_state']
										   ),
										  array(
													'tenant_apt_data_tenantid' => $tenant_id
												)
											);
					if($tst==true)
						{
					       
						$this->Flash->success(__('Tenant State Update Success', true));
				        $this->redirect(array('action'=>'index'));
			  } else {
				      
						$this->Flash->error(__('Error in Tenant State Update', true));
				         $this->redirect(array('action'=>'index'));
			   }
		  }

			$this->loadModel('Tenants');
          $tenant_name=$this->Tenants->getTenantFirstName($this->request->query('id'));
	    $this->set('tenant_name',$tenant_name);
	        $this->set('breadcrumb', "Chane Tenant State for".$tenant_name);	
			$this->set('tenantid',$tenant_id);
			$this->set('tenant_id',$tenant_id);
			
			$this->set('tenantaptid',$tenant_apt_id);	
		
			
     	     
		}
  }
?>