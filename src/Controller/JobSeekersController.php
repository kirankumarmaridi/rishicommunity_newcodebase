<?php

namespace App\Controller;

    class JobSeekersController extends AppController {
    $this->Careers = TableRegistry::get('Careers');
	$this->Jobseekersadmincomment = TableRegistry::get('Jobseekersadmincomment');
	public function calculate($max_age=null,$min_age=null,$sam_text=null){
		 $currentYear = date("Y");
		 $currentMonth = date("M");
		 $currentDate = date("D");
		 $maxi_age = $max_age;
		 $mini_age = $min_age;
		 $fromDate = date($currentYear - $max_age .' - ' .$currentMonth .' - '. $currentDate);
         $toDate = date($currentYear - $min_age .' - ' .$currentMonth .' - '. $currentDate);
		 $text = $sam_text;
		 
		 $ageConditionType = 'AND';
		 $ageCondition = array();
		 if(!empty($max_age)) {
		 	$ageCondition['JobSeekers.jobseeker_dob >= '] = $fromDate;
		 }
		 if(!empty($min_age)) {
		 	$ageCondition['JobSeekers.jobseeker_dob <= '] = $toDate;
		 }
		 			
		 $txtConditionType = 'OR';
    	 $txtCondition = array();
		 if(!empty($min_age)) {
		 	$txtCondition['JobSeekers.jobseeker_firstname LIKE'] = '%'.$text.'%';
		 	$txtCondition['JobSeekers.jobseeker_lastname LIKE'] = '%'.$text.'%';
		 }
		 				
		 $aronditionType = 'AND';
		 $aronditions = array(
		 					$ageConditionType => $ageCondition,
		 					$txtConditionType => $txtCondition
		 				);
		 
		 $this->paginate = array(
								   'JobSeekers' => array(
		 								                  'fields' => array(
														                    'JobSeekers.*, 
														                     Careers.career_title'
													                   ),
										                  'joins' => array(
														                    array(
															                      'table' => 'careers', 
															                      'alias' => 'Careers',
															                      'type' => 'LEFT',									
															                      'conditions' => array(
																             'Careers.career_id = JobSeekers.jobseeker_careerid'
															                                      )
														                     )
													                   ),
														  
								                          'conditions' => array( 
														                         // 'JobSeekers.JobSeekers_current_ctc between ? and ?' => array(1000,15000),
																				 // 'JobSeekers.JobSeekers_expected_ctc between ? and ?' => array(5000,20000),
																				 
																		        'AND' => $arrConditions
																	),
															
														)
						   );	
		// Query to get all the JobSeekerss			
		 $jobseekers= $this->paginate('JobSeekers');
		 $this->set('jobseekers', $jobseekers);
	}
	public function index()
	{
        $this->calculate();
	}
	public function download() {
        $this->viewClass = 'Media';
		
		$resume = $this->Jobseekers->find('first',
    							             array(
    								               'fields'     => 'jobseeker_resume',
    								               'conditions' => array(
												                         'jobseeker_id' => $this->params['named']['id']
															       )
    							             )
    						     );
    						     
        $params = array(
            'id'        => $resume['Jobseekers']['jobseeker_resume'],
            'name'      => $resume['Jobseekers']['jobseeker_resume'],
            'download'  => true,
            'path'      => RESUMES_FOLDER. DS
        );
        $this->set($params);
    }
      public function details() {
    	$this->layout = 'admin';
    	$jobseekers = $this->Jobseekers->find('first', array(
    												'conditions' => array(
    													'jobseeker_id' => $this->params['named']['id']
    												)
    											)
    								);
       $this->set('career_title', $this->Careers->getCareerTitle($jobseekers->jobseeker_careerid)); 
	  	$this->set('jobseekername', $jobseekers->jobseeker_firstname.' '.$jobseeker->jobseeker_lastname);
    	$this->set('jobseekerid', $this->params['named']['id']);
    	$this->set('jobseekers', $jobseekers);
    	    	
		// setting breadcrumb value
		// setting pagetitle
		$pageTitle = SITE_NAME.' : '.JOBSEEKERS;
		$this->set('title_for_layout', $pageTitle);
    	
    }
}
?>