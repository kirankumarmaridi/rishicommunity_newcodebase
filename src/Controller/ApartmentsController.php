<?php
  namespace App\Controller;
  use Cake\Event\Event;

    class ApartmentsController extends AppController { 
	
    public $uses = array('Apartments','Leasings','Rents');
	public function beforeFilter(Event $event) {
		
	  	 
		parent::beforeFilter($event); 
		$this->loginAction = array(
										'controller' => 'admins',
										'action' => 'login',
										'plugin' => null
									);
									 $this->viewBuilder()->layout("admin"); 
	 }
	
	public function index()
	{
		
        $this->set('apartments',$this->Apartments->find('all'));
		$this->set('breadcrumb','Apartments');
	}
	public function add()
	  {
			$apartments=$this->Apartments->newEntity();
			     if ($this->request->is('post')) {
                  $apartment= $this->Apartments->patchEntity($apartments, $this->request->data);
			     if($this->Apartments->save($apartments)){
			    	$this->Flash->success(__('Your detail has been saved.'));
					return $this->redirect(array('action'=>'index'),null, true);
				} else {
			    	 $this->redirect(array('action'=>'index'));
				}
		  }
	  }
	  public function edit($id)
	  {
			$apartments=$this->Apartments->get($id);
				 if ($this->request->is(['patch', 'post', 'put'])) {
                   $apartments= $this->Apartments->patchEntity($apartments, $this->request->data);
			        if($this->Apartments->save($apartments)){
			    	$this->Flash->success(__('Your detail has been updated.'));
					return $this->redirect(['action'=>'index']);
				} 
				$this->Flash->error(__('Unable to update your post.'));
				}   
		   $this->set('apartments',$apartments);
		      $this->set('breadcrumb', 'Edit Apartment Details');
	  }
	  public function delete($id= null)
	  {
	  	 $apartment= $this->Apartments->get($id);
          if ($this->Apartments->delete($apartment)) {
          $this->Flash->success(__('Apartment Is Deleted.'));
           return $this->redirect(['action' => 'index']);
      }
	 }
	 public function view()
	{
		 $query= $this->Apartments->find('all',
			 							       array(
											          'conditions' => array(  
													                        'apt_id'=>$this->request->query('id')
																	  )
											   )
								  );
			$apartment=$query->first();
		$this->set('apartment', $apartment);
		 $this->set('breadcrumb', "Apartment details of ".$apartment->apt_name);
	}
		public function getCurrentLeaseEndDateOfApartment($apt_id) {
		 $this->loadModel('Leasings');
		$this->response->body(json_encode($this->Leasings->getCurrentLeaseEndDateOfApartment($apt_id)));
		 return $this->response;
	}
		public function getCurrentRentDateOfApartment($apt_id) {
		$this->loadModel('Rents');
		$this->response->body(json_encode($this->Rents->getCurrentRentDateOfApartment($apt_id)));
		 return $this->response;
	}
		
	 
}
?>