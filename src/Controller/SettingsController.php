<?php 
  namespace App\Controller;

use Cake\Event\Event;

use Cake\ORM\TableRegistry;
  class SettingsController extends AppController {
  	
public $uses = array('Abouts', 'Contacts');
	 public function beforeFilter(Event $event) {
		
	  	 
		parent::beforeFilter($event); 
		$this->loginAction = array(
										'controller' => 'admins',
										'action' => 'login',
										'plugin' => null
									);
									 $this->viewBuilder()->layout("admin"); 
	 }
	  public function editabout()
	  {
	     $this->loadModel('Abouts');  
	  	$about=$this->Abouts->get($this->request->query('id'));
		 if ($this->request->is(['patch', 'post', 'put'])) {
		 $abouts= $this->Abouts->patchEntity($about, $this->request->data);
		 if($this->Abouts->save($abouts)) {
			  $this->Flash->success('The Changes has been saved');
				
			  } else {
				  $this->Flash->error('The Changes could not be saved. Please, try again.');
			  }
		}
		 $this->set('about', $about);
     	$this->set('breadcrumb','Edit About');
		}
		 public function editcontact()
	  {
	     $this->loadModel('Contacts');  
	  	$contact=$this->Contacts->get($this->request->query('id'));
		 if ($this->request->is(['patch', 'post', 'put'])) {
		$contacts= $this->Contacts->patchEntity($contact, $this->request->data);
		 if($this->Contacts->save($contacts)) {
			  $this->Flash->success('The Changes has been saved');
				
			  } else {
				  $this->Flash->error('The Changes could not be saved. Please, try again.');
			  }
		}
		 $this->set('contact', $contact);
     	$this->set('breadcrumb','Edit Contact');
		}
}
?>