<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\Event\Event;


use Cake\ORM\TableRegistry;

class RentsController extends AppController
{
public function beforeFilter(Event $event) {
		
	  	 
		parent::beforeFilter($event); 
		$this->loginAction = array(
										'controller' => 'admins',
										'action' => 'login',
										'plugin' => null
									);
									 $this->viewBuilder()->layout("admin"); 
	 }
   public $uses=array('rents');
public function index()
	{
		if(!empty($this->request->query('apid'))) {
		$this->loadModel("Apartments");
			$apartment_name = $this->Apartments->getApartmentName($this->request->query('apid'));
			$rents=$this->Rents->find('all')
			                      /*  ->select()
			                        ->select('Apartments.apt_name')*/
			                        ->join([
			                                 [
					                          'table'=>'apartments',
						                      'alias'=>'Apartments',
						                      'type'=>'LEFT',
						                      'conditions'=>([
						                                       'Apartments.apt_id= Rents.rent_apartment_id'
								                             ])
								             ]
								          ])
										  ->where([
											           [
														(['rent_apartment_id' => $this->request->query('apid')])
														]
												]);
		   $this->set('rents',$rents);		  
			$this->set('apartment_name', $apartment_name);
			/*$this->set('leases', $query);*/
			$this->set('apartment_id',$this->request->query('apid'));
			$this->set('breadcrumb', 'Rents'." for ".$apartment_name );
		} else {
			$this->redirect(array('controller' => 'apartments', 'action'=>'index'));
		}
	}
	public function add()
	  {
	    $this->loadModel("Apartments");
			$apartment_name = $this->Apartments->getApartmentName($this->request->query('id'));  
   	       $rents = $this->Rents->newEntity();
		   $rents->rent_apartment_id = $this->request->query('id');
			     if ($this->request->is('post')) {
                  $rents= $this->Rents->patchEntity($rents, $this->request->data);	 
			     if($this->Rents->save($rents)){
			    	$this->Flash->success(__('Your detail has been saved.'));
					return $this->redirect(array('action'=>'index'),null, true);
				} else {
			    	 $this->redirect(array('action'=>'index'));
				}
		  }
		  	$this->set('apartment_name', $apartment_name);
			/*$this->set('leases', $query);*/
			$this->set('apartment_id',$this->request->query('id'));

		} 
		 public function edit()
	  { 
               $this->loadModel("Apartments");
			$apartment_name = $this->Apartments->getApartmentName($this->request->query('id'));	  
       			$rents=$this->Rents->get($this->request->query('rentid'));
				 if ($this->request->is(['patch', 'post', 'put'])) {
                     $rents= $this->Rents->patchEntity($rents, $this->request->data);
			     if($this->Rents->save($rents)){
			    	$this->Flash->success(__('Your detail has been saved.'));
					return $this->redirect(array('action'=>'index'),null, true);
				} else {
			    	 $this->redirect(array('action'=>'index'));
				}
		  }
		  	$this->set('apartment_name', $apartment_name);
			$this->set('apartment_id',$this->request->query('id'));
		 
		   $this->set('rents',$rents);
		   $this->set('breadcrumb', 'Edit Rent Details');
	}	  		
   }
?>