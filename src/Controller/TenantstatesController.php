<?php

namespace App\Controller;

use Cake\Event\Event;


    class TenantstatesController extends AppController {
	public $uses=array('Tenantstates');
	  
 	public function beforeFilter(Event $event) {
		
	  	 
		parent::beforeFilter($event); 
		$this->loginAction = array(
										'controller' => 'admins',
										'action' => 'login',
										'plugin' => null
									);
									 $this->viewBuilder()->layout("admin"); 
	 }

	public function index()
	{
	$this->loadModel('Tenantstates');
		$tenantStates= $this->Tenantstates->find('all');
        $this->set(compact('tenantStates'));
			$this->set('breadcrumb','Tenantstates');
		
	}
	public function add()
	  {
	  $this->loadModel('Tenantstates');
			     $tenantStates=$this->Tenantstates->newEntity();
			     if ($this->request->is('post')) {
                  $tenantStates= $this->Tenantstates->patchEntity( $tenantStates, $this->request->data);
			     if($this->Tenantstates->save($tenantStates)){
			    	$this->Flash->success(__('Tenant added successfully.'));
					return $this->redirect(array('action'=>'index'),null, true);
				} else {
			    	 $this->redirect(array('action'=>'index'));
				}
		  }
		}
		 public function edit($id)
	  { 
	            $this->loadModel('Tenantstates');
    			$tenantStates=$this->Tenantstates->get($id);
				 if ($this->request->is(['patch', 'post', 'put'])) {
                    $tenantState= $this->Tenantstates->patchEntity($tenantStates, $this->request->data);
			        if($this->Tenantstates->save($tenantState)){
			    	$this->Flash->success(__('Your detail has been updated.'));
					return $this->redirect(['action'=>'index']);
				} 
				$this->Flash->error(__('Unable to update your post.'));
				}   
		   $this->set('tenantStates', $tenantStates);
		    $this->set('breadcrumb', 'Edit TenantState Details');
	}	  		
		  public function delete($id)
    {
	    $this->loadModel('Tenantstates');
      $tenantStates= $this->Tenantstates->get($id);
      if ($this->Tenantstates->delete( $tenantStates)) {
        $this->Flash->success(__('Tenant Is Deleted.'));
        return $this->redirect(['action' => 'index']);
      }
	 } 
}
?>