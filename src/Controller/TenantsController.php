<?php

namespace App\Controller;

use Cake\Event\Event;

    class TenantsController extends AppController {
	
  public function beforeFilter(Event $event) {
		
	  	 
		parent::beforeFilter($event); 
		$this->loginAction = array(
										'controller' => 'admins',
										'action' => 'login',
										'plugin' => null
									);
									 $this->viewBuilder()->layout("admin"); 
	 }
	public function index()
	{
	 	
	   $tenant= $this->Tenants->find('all');
        $this->set(compact('tenant'));
			$this->set('breadcrumb','Tenants');
		
	}
	public function add()
	  {
	    $tenant=$this->Tenants->newEntity();
			     if ($this->request->is('post')) {
                   $tenant= $this->Tenants->patchEntity( $tenant, $this->request->data);
			     if($this->Tenants->save( $tenant)){
			    	$this->Flash->success(__('Your detail has been saved.'));
					return $this->redirect(array('action'=>'index'),null, true);
				} else {
			    	 $this->redirect(array('action'=>'index'));
				}
		  }
	  }
	 
	    public function edit($id)
	  {
		  $tenant=$this->Tenants->get($id);
				 if ($this->request->is(['patch', 'post', 'put'])) {
                    $tenant= $this->Tenants->patchEntity($tenant, $this->request->data);
			        if($this->Tenants->save($tenant)){
			    	$this->Flash->success(__('Your detail has been updated.'));
					return $this->redirect(['action'=>'index']);
				} 
				$this->Flash->error(__('Unable to update your post.'));
				}   
		   $this->set('tenant', $tenant);
		   $this->set('breadcrumb', 'Edit Tenant Details');
	}	  	
	  public function delete($id)
    {
       $tenant=$this->Tenants->get($id);
      if ($this->Tenants->delete( $tenant)) {
        $this->Flash->success(__('Tenant Is Deleted.'));
        return $this->redirect(['action' => 'index']);
      }
	 } 	
}
?>	