<?php

namespace App\Controller;

use Cake\Event\Event;

use Cake\Mailer\Email;
use Cake\Datasource\ConnectionManager;

class AdminsController extends AppController
{
	var $name = "Admins";
//	/*/*public $helpers = array('Html', 'Form', 'Session', 'Js' => array('Jquery'), 'Paginator');*/
   /* var $components = array('Acl', 'Auth', 'Email');*/
    public $uses = array('Admins');
	
 public function beforeFilter(Event $event) {
		
	  	 
		parent::beforeFilter($event); 
		$this->loginAction = array(
										'controller' => 'admins',
										'action' => 'login',
										'plugin' => null
									);
		$this->Auth->allow('register', 'forgotpassword', 'changepassword'); 
							
			 $this->viewBuilder()->layout("admin"); 
	 }

    function index() {
	  
		$this->redirect('login');	
    }
     function register()
    {
        if ($this->request->is('post')) {	
			$conn = ConnectionManager::get('default');
    		$save=$conn->query ("INSERT INTO `admins` (`admin_firstname`, `admin_lastname`, `admin_email`, `admin_password`, `admin_mobile`) VALUES ( '".$this->request->data['admin_firstname']."', '".$this->request->data['admin_lastname']."','".$this->request->data['admin_email']."','".md5($this->request->data['admin_password'])."', '".$this->request->data['admin_mobile']."')");
        	
            if ($save) {
                $this->Flash->success(__('Your account has been created now u can login.', true));
                $this->redirect('/admins/login');
            } else {
                $this->Flash->error(__('Your account could not be created.', true));
            }
        }
		$this->viewBuilder()->layout('admin');
    }
    function login()
    { 

    	// Check for a successful login
        if (!empty($this->request->data)) {
        	$result = $this->Admins->find('all', 
        									array(
        										'conditions' => array(
        															'admin_email' => $this->request->data['admin_email'],
        															'admin_password' => md5($this->request->data['admin_password'])
        															)
        										)
        								);
        		$row=$result->first();
										
            if (!empty($row)) {
		     $this->loadComponent('Auth');
            $this->Auth->setUser($this->request->data);
            	$this->request->session()->write('user_id', 
													$row->admin_id
												); // Writing user id in session
            	$this->request->session()->write('user_name', 
													$row->admin_firstname." ".$row->admin_lastname
												); // Writing user id in session
                $this->redirect(array('action'=>'account'),null, true);
            } else {
            	$this->Flash->error('Please check your credentials.');
				/*return $this->redirect(array('action'=>'login'),null, true);*/
            }
        }
		
    }

    /**
     * Log a admin out
     */
    function logout()
    {
	  $this->request->session()->destroy();
      /*$this->loadComponent('Auth');*/
	  $this->loadComponent('Auth');
	  return $this->redirect($this->Auth->logout());
    }

    /**
     * Account details page (change password)
     */
    function account()
    {
    	if($this->request->session())
    	{
	        // Set Admin's ID in model which is needed for validation
	        $this->Admins->id = $this->request->session()->read('user_id');
	
	        // Load the admin (avoid populating $this->data)
	        $current_admin = $this->Admins->find('all', array(
	        													'conditions' => array(
	        														'admin_id' => $this->request->session()->read('user_id')
	        													)
	        												)
	        									);
			$row=$current_admin->first();
	        $this->set('current_admin', $row);
	        
	        // setting breadcrumb values
			$this->set('breadcrumb','Home Page');
			
	        // setting layout
			$this->viewBuilder()->layout('admin_dashboard');
    	}
		else {
    		$this->redirect(array('action'=>'login'));
    	}


    }
	
	 public function changeprofile() 
	{		
		// user id from the session
		$admin_id = $this->request->session()->read('user_id');
		// Setting the data
	//	$this->set($this->request->data);
		$query=$this->Admins->get($admin_id);
		 if ($this->request->is(['patch', 'post', 'put'])) {
		$admin= $this->Admins->patchEntity($query, $this->request->data);
		 if($this->Admins->save($admin)) {		
				$this->Flash->success(__('profile Changed Successfully', true));
			} else { 
				//failure
				$this->Flash->error(__('Profile Error', true));
			}
			
		}

       $this->set('c_prof',$query);
		$username = $this->request->session()->read('user_name');
		$pageTitle = $username.' : '.'My Profile';
		$this->set('title', $pageTitle);		
	}// end of changeprofile()
    
    
     public function changepassword() 
	{
		// assigning the session userid
		$admin_id = $this->request->session()->read('user_id');
        $this->Admins->id =$admin_id;
		// Setting the viadmin data
		$this->set($this->request->data);
		
		// if form data is not empty
		if($this->request->is('post')) 
		{						
				// if both passwords are same
				if($this->Admins->validatePasswdConfirm($this->request->data)) 
				{		
					// encrypting the given password
					$password = md5($this->request->data['admin_password']);
					$conn = ConnectionManager::get('default');
					// query to update the password for viadmin
					$pswd=$conn->query("UPDATE admins SET admin_password='".$password."' WHERE admin_id = ".$admin_id);
			

					// success message
					$this->Flash->success(__('Password Changed Successfully', true));
					// redirecting to the same page after the password is changed
					$this->redirect('/admins/changepassword');                
				} else {
					// if both the passwords are not same
					$this->Flash->error(__('Password Must Same', true));			
				}			
			
		}
		// setting breadcrumb value
		$this->set('breadcrumb','Change Password');
		// setting pagetitle
		$username = $this->request->session()->read('user_name');
	
		$pageTitle = $username.' : '.'Change Password';
		$this->set('title', $pageTitle);
		
	}// end of changepassword()
    /**
     * Allows the admin to email themselves a password redemption token
     */  	
}
