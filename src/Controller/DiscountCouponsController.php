<?php
  namespace App\Controller;
  
  use Cake\Event\Event;

    class DiscountCouponsController extends AppController { 
	
    public $uses = array('DiscountCoupons');	
	 public function beforeFilter(Event $event) {
		
	  	 
		parent::beforeFilter($event); 
		$this->loginAction = array(
										'controller' => 'admins',
										'action' => 'login',
										'plugin' => null
									);
									 $this->viewBuilder()->layout("admin"); 
	 }  
	public function index()
	{
		
        $this->set('discountcoupons',$this->DiscountCoupons->find('all'));
		$this->set('breadcrumb','DiscountCoupons');
	}
	public function add()
	  {
			$discountcoupons=$this->DiscountCoupons->newEntity();
			     if ($this->request->is('post')) {
                  $discountcoupon= $this->DiscountCoupons->patchEntity($discountcoupons, $this->request->data);
			     if($this->DiscountCoupons->save($discountcoupons)){
			    	$this->Flash->success(__('Your detail has been saved.'));
					return $this->redirect(array('action'=>'index'),null, true);
				} else {
			    	 $this->redirect(array('action'=>'index'));
				}
		  }
	  }
	  public function edit($id)
	  {
			$discountcoupons=$this->DiscountCoupons->get($id);
				 if ($this->request->is(['patch', 'post', 'put'])) {
                    $rent= $this->DiscountCoupons->patchEntity($discountcoupons, $this->request->data);
			        if($this->DiscountCoupons->save($discountcoupons)){
			    	$this->Flash->success(__('Your detail has been updated.'));
					return $this->redirect(['action'=>'index']);
				} 
				$this->Flash->error(__('Unable to update your post.'));
				}   
		   $this->set('discountcoupons',$discountcoupons);
		   $this->set('breadcrumb', 'Edit DiscountCoupons');
	  }
	  public function delete($id= null)
	  {
	  	 $discountcoupons= $this->DiscountCoupons->get($id);
          if ($this->DiscountCoupons->delete($discountcoupons)) {
          $this->Flash->success(__('Discountcoupon Is Deleted.'));
           return $this->redirect(['action' => 'index']);
      }
	 }
}
?>