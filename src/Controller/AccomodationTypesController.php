<?php

namespace App\Controller;
use Cake\Event\Event;

    class AccomodationTypesController extends AppController {

	 public function beforeFilter(Event $event) {
		
	  	 
		parent::beforeFilter($event); 
		$this->loginAction = array(
										'controller' => 'admins',
										'action' => 'login',
										'plugin' => null
									);
			$this->viewBuilder()->layout("admin"); 
	 }
	
	public function index()
	{
		$accomodation= $this->AccomodationTypes->find('all');
        $this->set(compact('accomodation'));
		$this->set('breadcrumb','AccomodationTypes');
	}
     public function add()
	  {
			     $accomodation=$this->AccomodationTypes->newEntity();
			     if ($this->request->is('post')) {
                   $accomodation= $this->AccomodationTypes->patchEntity( $accomodation, $this->request->data);
			     if($this->AccomodationTypes->save($accomodation)){
			    	$this->Flash->success(__('Your detail has been saved.'));
					return $this->redirect(array('action'=>'index'),null, true);
				} else {
			    	 $this->redirect(array('action'=>'index'));
				}
		  }
		 }
		
		
	 public function edit($id)
	  { 
    			$accomodation=$this->AccomodationTypes->get($id);
				 if ($this->request->is(['patch', 'post', 'put'])) {
                    $accomodation= $this->AccomodationTypes->patchEntity($accomodation, $this->request->data);
			        if($this->AccomodationTypes->save($accomodation)){
			    	$this->Flash->success(__('Your detail has been updated.'));
					return $this->redirect(['action'=>'index']);
				} 
				$this->Flash->error(__('Unable to update your post.'));
				}   
		   $this->set('accomodation', $accomodation);
		   $this->set('breadcrumb', 'Edit AccomodationTypes Details');
	}	  		
		  public function delete($id)
    {
      $accomodation= $this->AccomodationTypes->get($id);
      if ($this->AccomodationTypes->delete( $accomodation)) {
        $this->Flash->success(__('Accomodation Type Is Deleted.'));
        return $this->redirect(['action' => 'index']);
      }
	 } 
}
?>