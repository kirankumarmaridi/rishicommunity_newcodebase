<?php
    namespace App\Controller;
	use Cake\Event\Event;

	
   class StaffUsersController extends AppController {
   public function beforeFilter(Event $event) {
		
	  	 
		parent::beforeFilter($event); 
		$this->loginAction = array(
										'controller' => 'admins',
										'action' => 'login',
										'plugin' => null
									);
									 $this->viewBuilder()->layout("admin"); 
	 }
    public function index()
	{
		$this->loadModel('Admins');
	   $staffusers= $this->Admins->find('all');
       $this->set(compact('staffusers'));
	}
	 public function add()
	  {
		     $this->loadModel('Admins');
			 $staffusers=$this->Admins->newEntity();
		     if ($this->request->is('post')) {
                   $staffusers= $this->Admins->patchEntity( $staffusers, $this->request->data);
			     if($this->Admins->save($staffusers)){
			    	$this->Flash->success(__('StaffUser Added Successfully.'));
					return $this->redirect(array('action'=>'index'),null, true);
				} else {
			    	 $this->redirect(array('action'=>'index'));
				}
		  }
		 }
 public function changeStaffStatus($c_id) 
	  {
	  $this->loadModel('Admins');
	  	if (!$c_id) 
		{
			$this->Flash->success(__(INVALID_ID_FOR_STAFFUSER, true));
			$this->redirect(array('action'=>'index'));
		} else {
		
			$query = $this->Admins->find('all',
			                                   array(
											          'conditions' => array(  
													                        'admin_id'=> $c_id
																	  )
											   )
			                               ); // getting all the data from the projects table for the given $c_id 
			// Here changing the project status
			
			$data=$query->first();
			if($data->admin_status== 1)
			{
				$data->admin_status = 0; // Deactivating StaffUser
				$status = 'Disabled';
			} else {
				$data->admin_status= 1; // Activating StaffUser
				$status = 'Enabled';
			}
			// Saving the StaffUser changed status
			if ($this->Admins->save($data)) {
				$this->Flash->success(__('StaffUser'.' ('.$data->admin_firstname.')'.'Has Been '.$status, true));
				//$this->redirect(array('action'=>'index'));
				$this->redirect($_SERVER['HTTP_REFERER']);
			}
		}
	  }
	}
	?>