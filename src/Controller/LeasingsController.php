<?php
   namespace App\Controller;
	
	use Cake\Event\Event;
   class LeasingsController extends AppController {
 
  public function beforeFilter(Event $event) {
		
	  	 
		parent::beforeFilter($event); 
		$this->loginAction = array(
										'controller' => 'admins',
										'action' => 'login',
										'plugin' => null
									);
									 $this->viewBuilder()->layout("admin"); 
	 }
	public function index()
  {
     $uses = array('leasings');
		if(!empty($this->request->query('id'))) {
		$this->loadModel("Apartments");
			$apartment_name = $this->Apartments->getApartmentName($this->request->query('id'));
			$ls=$this->Leasings->find('all')
			                      /*  ->select()
			                        ->select('Apartments.apt_name')*/
			                        ->join([
			                                 [
					                          'table'=>'apartments',
						                      'alias'=>'Apartments',
						                      'type'=>'LEFT',
						                      'conditions'=>([
						                                       'Apartments.apt_id= Leasings.lease_apartment_id'
								                             ])
								             ]
								          ])
										  ->where([
											           [
														(['lease_apartment_id' =>$this->request->query('id')])
														]
												]);
		   $this->set('leasings',$ls);		  
			$this->set('apartment_name', $apartment_name);
			/*$this->set('leases', $query);*/
			$this->set('apartment_id', $this->request->query('id'));
			$this->set('breadcrumb','Leasings'." for ".$apartment_name );
		} else {
			$this->redirect(array('controller' => 'apartments', 'action'=>'index'));
		}
	
	}
	public function add()
	  {
           $this->loadModel("Apartments");	  
           $apartment_name = $this->Apartments->getApartmentName($this->request->query('id'));	  
	       $leasing = $this->Leasings->newEntity();
         /*  $article->title = 'An article by mark';
			$leases=$this->Leasings->newEntity();*/
			$leasing->lease_apartment_id =  $this->request->query('id');
			     if ($this->request->is('post')) {
                  $leases= $this->Leasings->patchEntity($leasing, $this->request->data);
			     if($this->Leasings->save($leases)){
			    	$this->Flash->success(__('Your detail has been saved.'));
					return $this->redirect(array('action'=>'index'),null, true);
				} else {
			    	 $this->redirect(array('action'=>'index'));
				}
		  }
		  	$this->set('apartment_id', $this->request->query('id'));
				$this->set('apartment_name', $apartment_name);
		} 

		

	 public function edit()
	  { 
	  $this->loadModel("Apartments");	  
           $apartment_name = $this->Apartments->getApartmentName($this->request->query('id'));	  
    			$leases=$this->Leasings->get($this->request->query('leaseid'));
				 if ($this->request->is(['patch', 'post', 'put'])) {
                     $lease= $this->Leasings->patchEntity($leases, $this->request->data);
			     if($this->Leasings->save($lease)){
			    	$this->Flash->success(__('Your detail has been saved.'));
					return $this->redirect(array('action'=>'index'),null, true);
				} else {
			    	 $this->redirect(array('action'=>'index'));
				}
		  }
		  $this->set('apartment_id', $this->request->query('id'));
				$this->set('apartment_name', $apartment_name);
		   $this->set('leases',$leases);
		   $this->set('breadcrumb', 'Edit lease');
	}	  		

}	
?>