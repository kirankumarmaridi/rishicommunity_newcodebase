<?php
    namespace App\Controller;
	
	use Cake\Event\Event;

   class BillTypesController extends AppController {
   
   public function beforeFilter(Event $event) {
		
	  	 
		parent::beforeFilter($event); 
		$this->loginAction = array(
										'controller' => 'admins',
										'action' => 'login',
										'plugin' => null
									);
									 $this->viewBuilder()->layout("admin"); 
	 }
    public function index()
	{
	   $billtypes= $this->BillTypes->find('all');
       $this->set(compact('billtypes'));
	   $this->set('breadcrumb','Billtypes');
	}
	 public function add()
	  {
		     $billtypes=$this->BillTypes->newEntity();
		     if ($this->request->is('post')) {
                  $billtypes= $this->BillTypes->patchEntity( $billtypes, $this->request->data);
			     if($this->BillTypes->save($billtypes)){
			    	$this->Flash->success(__('Bill Added Successfully.'));
					return $this->redirect(array('action'=>'index'),null, true);
				} else {
			    	 $this->redirect(array('action'=>'index'));
				}
		  }
		   $this->set('breadcrumb', 'Create Billtype');
		 }
		  public function edit($id)
	  { 
    			 $billtype=$this->BillTypes->get($id);
				 if ($this->request->is(['patch', 'post', 'put'])) {
                    $billtypes= $this->BillTypes->patchEntity($billtype, $this->request->data);
			        if($this->BillTypes->save($billtypes)){
			    	$this->Flash->success(__('Bill Type has been updated.'));
				return $this->redirect(['action'=>'index']);
				} 
				$this->Flash->error(__('Unable to update your post.'));
			}   
		   $this->set('billtype',$billtype);
		   $this->set('breadcrumb', 'Edit BillType Details');
	}	  		
		  public function delete($id)
          {
           $billtypes= $this->BillTypes->get($id);
         if ($this->BillTypes->delete($billtypes)) {
        $this->Flash->success(__('Bill Type Is Deleted.'));
       return $this->redirect(['action' => 'index']);
      }
	 } 
}
 ?>