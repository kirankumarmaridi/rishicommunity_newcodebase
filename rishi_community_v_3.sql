-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2015 at 06:15 AM
-- Server version: 5.6.26
-- PHP Version: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rishi_community_v_3`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE IF NOT EXISTS `abouts` (
  `about_id` int(10) NOT NULL,
  `about_data` longtext
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`about_id`, `about_data`) VALUES
(1, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc..');

-- --------------------------------------------------------

--
-- Table structure for table `accomodation_types`
--

CREATE TABLE IF NOT EXISTS `accomodation_types` (
  `accomodation_type_id` int(11) NOT NULL,
  `accomodation_type_name` varchar(255) NOT NULL,
  `accomodation_type_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accomodation_types`
--

INSERT INTO `accomodation_types` (`accomodation_type_id`, `accomodation_type_name`, `accomodation_type_status`) VALUES
(1, 'Male', 1),
(2, 'Female', 1),
(3, 'Family', 1);

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `admin_status` tinyint(4) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `admin_firstname` varchar(100) NOT NULL,
  `admin_lastname` varchar(100) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `admin_password` varchar(100) NOT NULL,
  `admin_mobile` varchar(20) NOT NULL,
  `admin_created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_status`, `admin_id`, `admin_firstname`, `admin_lastname`, `admin_email`, `admin_password`, `admin_mobile`, `admin_created_date`) VALUES
(1, 1, 'vineela', 'appasani', 'vineela.appasani@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '9703456817', '2015-06-30 23:14:25'),
(0, 4, 'Rishi', 'Community', 'rishi.community@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '', '2015-07-24 02:56:27'),
(0, 9, 'kavya', 'vivek', 'kayva@gmail.com', 'e7476ca55559e029965da4e8f1018e45', '9966140401', '2015-11-12 04:26:59'),
(0, 10, 'shabana', 'sheik', 'shabana.sheik.edit@gmail.com', 'cfa05a88a9005313aa12b3458e85e48a', '7416503385', '2015-11-12 04:54:08'),
(0, 11, 'gayatri', 'thurangi', 'gayatri@gmail.com', 'edf8377563f7fa2897df6aa434ad305e', '8978648315', '2015-11-12 12:03:50'),
(0, 12, 'munna', 'sheik', 'munna@gmail.com', 'cf72c35762cb653212e7edebf8bd53d8', '9493817886', '2015-11-12 12:05:27');

-- --------------------------------------------------------

--
-- Table structure for table `apartments`
--

CREATE TABLE IF NOT EXISTS `apartments` (
  `apt_id` int(11) NOT NULL,
  `apt_name` varchar(100) NOT NULL,
  `apt_address` text NOT NULL,
  `apt_total_occupancy` int(2) NOT NULL,
  `apt_bedrooms` int(2) NOT NULL,
  `apt_bathrooms` int(2) NOT NULL,
  `apt_half_bathrooms` int(2) NOT NULL,
  `apt_car_parkings` int(2) NOT NULL,
  `apt_persons_required_on_lease` int(2) NOT NULL,
  `apt_persons_on_current_lease` int(2) NOT NULL,
  `apt_marketing_email` varchar(100) NOT NULL,
  `apt_bedroom1_occupancy` int(2) NOT NULL DEFAULT '0',
  `apt_bedroom2_occupancy` int(2) NOT NULL DEFAULT '0',
  `apt_bedroom3_occupancy` int(2) NOT NULL DEFAULT '0',
  `apt_bedroom4_occupancy` int(2) NOT NULL DEFAULT '0',
  `apt_living_room_occupancy` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `apartments`
--

INSERT INTO `apartments` (`apt_id`, `apt_name`, `apt_address`, `apt_total_occupancy`, `apt_bedrooms`, `apt_bathrooms`, `apt_half_bathrooms`, `apt_car_parkings`, `apt_persons_required_on_lease`, `apt_persons_on_current_lease`, `apt_marketing_email`, `apt_bedroom1_occupancy`, `apt_bedroom2_occupancy`, `apt_bedroom3_occupancy`, `apt_bedroom4_occupancy`, `apt_living_room_occupancy`) VALUES
(13, 'test', 'test1', 2, 2, 2, 2, 2, 2, 2, 'fhjhkjlk;l', 2, 2, 2, 2, 2),
(14, 'shabana', 'shabana', 2, 2, 2, 2, 2, 1, 2, 'jgkjhkk', 2, 2, 2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `bill_types`
--

CREATE TABLE IF NOT EXISTS `bill_types` (
  `bill_type_id` int(11) NOT NULL,
  `bill_type_name` varchar(255) NOT NULL,
  `bill_type_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bill_types`
--

INSERT INTO `bill_types` (`bill_type_id`, `bill_type_name`, `bill_type_status`) VALUES
(1, 'Rent', 1),
(2, 'Water', 1),
(3, 'Trash & Sewage', 1),
(4, 'Current Bill', 1),
(5, 'Internet', 1),
(6, 'Clicker/Fob', 1),
(7, 'Pedestrain Pass', 1),
(8, 'Other Charges', 1);

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

CREATE TABLE IF NOT EXISTS `careers` (
  `career_id` int(11) NOT NULL,
  `career_title` varchar(50) NOT NULL,
  `career_description` longtext NOT NULL,
  `career_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `careers`
--

INSERT INTO `careers` (`career_id`, `career_title`, `career_description`, `career_status`) VALUES
(1, 'PHP Developer', '<div>\r\n<ul>\r\n<li>Programing Languages required : PHP, HTML, Java script, MySQL</li>\r\n<li>Experience : 3 months to 1 year</li>\r\n<li>Frameworks : CakePhp</li>\r\n<li>Bond for 6 months to 1 year</li>\r\n<li>Joining Date : ASAP</li>\r\n<ul>\r\n</div>\r\n\r\n</br>', 1),
(2, 'QA Engineer', '<div>\r\n<ul>\r\n<li>Basic Knowledge in testing </li>\r\n<li>Understanding requirements</li>\r\n<li>Capable of writing test cases based on requirements</li>\r\n<li>Execute test cases and create bugs in JIRA</li>\r\n<li>Experience : 3 months to 1 year</li>\r\n<li>Frameworks : CakePhp</li>\r\n<li>Bond for 6 months to 1 year</li>\r\n<li>Joining Date : October</li>\r\n<ul>\r\n</div>\r\n\r\n</br>', 1),
(3, 'Tele-Marketing Executive-online in US Time zone', '<u><i><b><span style="font-size: small;">Your duties &amp; Salary:-</span></b></i></u><br>\r\nYour duty will be in two different shifts ( <span class="aBn" tabindex="0" data-term="goog_776323639"><span class="aQJ">10:00 am - 6:00 pm</span></span> (or) 7:00 pm - 2:30 am (or)  <span class="aBn" tabindex="0" data-term="goog_776323641"><span class="aQJ">2:30 am - 10:00 am IST</span></span>) nights only in Indian timings. After every three shifts one shift is off. 4 people will cover 24/7 duty. There is no holidays (three days shift duty one day off).\r\n<div>\r\n<ul>\r\n	<li>Your Salary will be Rs 5,000/- from the first/second month</li>\r\n	<li>Then it will be Rs 12,000/- from the second/third month onwards based on performance.</li>\r\n	<li>After six months 10% increase</li>\r\n	<li>minimum 6 months to 1 year bond commitment is required.</li>\r\n</ul>\r\n</div>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `contact_id` int(10) NOT NULL,
  `contact_address` text NOT NULL,
  `contact_email` text NOT NULL,
  `contact_phone` text NOT NULL,
  `contact_website` text NOT NULL,
  `contact_status` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`contact_id`, `contact_address`, `contact_email`, `contact_phone`, `contact_website`, `contact_status`) VALUES
(1, 'Bay Area,  United States America', 'rishi.community@gmail.com', '+1-925-289-6552', 'www.rishicommunity.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `discount_coupons`
--

CREATE TABLE IF NOT EXISTS `discount_coupons` (
  `discount_coupon_id` int(11) NOT NULL,
  `discount_coupon_code` varchar(255) NOT NULL,
  `discount_coupon_desc` varchar(255) NOT NULL,
  `discount_coupon_price` int(11) NOT NULL,
  `discount_coupon_amount_type` enum('Dollars','Percentage') NOT NULL DEFAULT 'Dollars',
  `discount_coupon_status` tinyint(1) NOT NULL DEFAULT '0',
  `discount_coupon_updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobseekers`
--

CREATE TABLE IF NOT EXISTS `jobseekers` (
  `jobseeker_id` int(11) NOT NULL,
  `jobseeker_careerid` int(11) NOT NULL,
  `jobseeker_firstname` varchar(255) NOT NULL,
  `jobseeker_lastname` varchar(255) NOT NULL,
  `jobseeker_dob` date NOT NULL,
  `jobseeker_current_ctc` int(11) NOT NULL,
  `jobseeker_expected_ctc` int(11) NOT NULL,
  `jobseeker_currentcity` varchar(255) NOT NULL,
  `jobseeker_mobilenumber` varchar(25) NOT NULL,
  `jobseeker_email_id` varchar(255) NOT NULL,
  `jobseeker_highest_qualification` varchar(255) NOT NULL,
  `jobseeker_highest_qualification_passed_out_year` year(4) NOT NULL,
  `jobseeker_available_joining_date` date NOT NULL,
  `jobseeker_total_experience` int(11) NOT NULL,
  `jobseeker_relavent_experince` int(11) NOT NULL,
  `jobseeker_resume` varchar(255) NOT NULL,
  `jobseeker_photo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobseekers`
--

INSERT INTO `jobseekers` (`jobseeker_id`, `jobseeker_careerid`, `jobseeker_firstname`, `jobseeker_lastname`, `jobseeker_dob`, `jobseeker_current_ctc`, `jobseeker_expected_ctc`, `jobseeker_currentcity`, `jobseeker_mobilenumber`, `jobseeker_email_id`, `jobseeker_highest_qualification`, `jobseeker_highest_qualification_passed_out_year`, `jobseeker_available_joining_date`, `jobseeker_total_experience`, `jobseeker_relavent_experince`, `jobseeker_resume`, `jobseeker_photo`) VALUES
(1, 1, 'testxcx', 'fsdfsdf', '1960-02-04', 0, 0, '', '', '', '', 0000, '0000-00-00', 0, 0, '', NULL),
(2, 1, 'dsfsdfsfd', 'fsdfsdftest', '1980-02-29', 0, 0, '', '', '', '', 0000, '0000-00-00', 0, 0, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jobseekers_admin_comments`
--

CREATE TABLE IF NOT EXISTS `jobseekers_admin_comments` (
  `jobseekers_admin_comment_id` int(11) NOT NULL,
  `jobseekers_admin_comment_jobseeker_id` int(11) NOT NULL,
  `jobseekers_admin_comment_user_id` int(11) NOT NULL,
  `jobseekers_admin_comment_comment` text NOT NULL,
  `jobseekers_admin_comment_parent_id` int(11) NOT NULL DEFAULT '0',
  `jobseekers_admin_comment_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `leasings`
--

CREATE TABLE IF NOT EXISTS `leasings` (
  `lease_id` int(11) NOT NULL,
  `lease_apartment_id` int(11) NOT NULL,
  `lease_start_date` date NOT NULL,
  `lease_end_date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leasings`
--

INSERT INTO `leasings` (`lease_id`, `lease_apartment_id`, `lease_start_date`, `lease_end_date`) VALUES
(1, 11, '2015-01-04', '2015-11-07'),
(2, 11, '2015-12-06', '2015-11-10'),
(3, 11, '2015-03-02', '2015-02-17'),
(4, 13, '2015-09-01', '2015-09-30'),
(5, 13, '2015-10-01', '2015-10-31'),
(6, 13, '2015-11-04', '2015-12-04');

-- --------------------------------------------------------

--
-- Table structure for table `rents`
--

CREATE TABLE IF NOT EXISTS `rents` (
  `rent_id` int(11) NOT NULL,
  `rent_apartment_id` int(11) NOT NULL,
  `rent_amount` int(11) NOT NULL,
  `rent_from_date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rents`
--

INSERT INTO `rents` (`rent_id`, `rent_apartment_id`, `rent_amount`, `rent_from_date`) VALUES
(1, 1, 123, '2015-09-09'),
(2, 11, 200, '2015-09-04'),
(3, 11, 700, '2015-10-01'),
(4, 11, 400, '2015-11-04'),
(5, 11, 8000, '2015-11-07'),
(6, 11, 9000, '2015-11-10'),
(7, 11, 400, '2015-11-12'),
(8, 11, 100, '2015-11-10'),
(9, 11, 900, '2015-11-10'),
(10, 13, 200, '2015-09-04'),
(11, 13, 300, '2015-10-01'),
(12, 13, 400, '2015-11-04'),
(13, 13, 600, '2015-11-05');

-- --------------------------------------------------------

--
-- Table structure for table `rents_for_users`
--

CREATE TABLE IF NOT EXISTS `rents_for_users` (
  `rents_for_user_id` int(11) NOT NULL,
  `rents_for_user_rent_apartment_id` int(11) NOT NULL,
  `rents_for_user_from_date` date NOT NULL,
  `rents_for_user_rent_amount` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rents_for_users`
--

INSERT INTO `rents_for_users` (`rents_for_user_id`, `rents_for_user_rent_apartment_id`, `rents_for_user_from_date`, `rents_for_user_rent_amount`) VALUES
(1, 1, '2015-11-12', 500),
(2, 11, '2015-11-06', 660),
(3, 11, '2015-01-17', 7777);

-- --------------------------------------------------------

--
-- Table structure for table `tenants`
--

CREATE TABLE IF NOT EXISTS `tenants` (
  `tenant_id` int(11) NOT NULL,
  `tenant_firstname` varchar(100) NOT NULL,
  `tenant_lastname` varchar(100) NOT NULL,
  `tenant_email` varchar(100) NOT NULL,
  `tenant_mobile` varchar(50) NOT NULL,
  `tenant_date_of_birth` date NOT NULL,
  `tenant_current_location` varchar(100) NOT NULL,
  `tenant_emergency_contact` varchar(50) NOT NULL,
  `tenant_created_by` int(11) NOT NULL,
  `tenant_created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tenants`
--

INSERT INTO `tenants` (`tenant_id`, `tenant_firstname`, `tenant_lastname`, `tenant_email`, `tenant_mobile`, `tenant_date_of_birth`, `tenant_current_location`, `tenant_emergency_contact`, `tenant_created_by`, `tenant_created_date`) VALUES
(9, 'xxx', 'yyy', 'aaa@gmail.com', '79898989898', '2015-11-10', 'vijayawada', '08916568768787', 0, '2015-11-10 14:57:10'),
(10, 'shabana', 'sheik', 'shabana@gmail.com', '7416503385', '2015-11-14', 'vizag', '08912797786', 0, '2015-11-11 11:01:56');

-- --------------------------------------------------------

--
-- Table structure for table `tenant_apt_data`
--

CREATE TABLE IF NOT EXISTS `tenant_apt_data` (
  `tenant_apt_data_id` int(11) NOT NULL,
  `tenant_apt_data_tenantid` int(11) NOT NULL,
  `tenant_apt_data_aptid` int(11) NOT NULL,
  `tenant_apt_data_joining_date` date NOT NULL,
  `tenant_apt_data_notice_date` date DEFAULT NULL,
  `tenant_apt_data_vacating_date` date DEFAULT NULL,
  `tenant_apt_data_state` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tenant_apt_data`
--

INSERT INTO `tenant_apt_data` (`tenant_apt_data_id`, `tenant_apt_data_tenantid`, `tenant_apt_data_aptid`, `tenant_apt_data_joining_date`, `tenant_apt_data_notice_date`, `tenant_apt_data_vacating_date`, `tenant_apt_data_state`) VALUES
(1, 1, 11, '2015-11-01', '2015-11-19', '2015-12-01', 1),
(2, 9, 13, '2015-11-10', '2015-11-16', '2015-11-03', 2),
(3, 10, 13, '2015-11-11', '2015-11-14', '2015-11-19', 4);

-- --------------------------------------------------------

--
-- Table structure for table `tenant_discount_data`
--

CREATE TABLE IF NOT EXISTS `tenant_discount_data` (
  `tenant_discount_data_id` int(11) NOT NULL,
  `tenant_discount_data_apt_data_id` int(11) NOT NULL,
  `tenant_discount_data_coupon_id` int(11) NOT NULL,
  `tenant_discount_data_coupon_start_date` date NOT NULL,
  `tenant_discount_data_coupon_end_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tenant_states`
--

CREATE TABLE IF NOT EXISTS `tenant_states` (
  `tenant_state_id` int(11) NOT NULL,
  `tenant_state_name` varchar(255) NOT NULL,
  `tenant_state_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tenant_states`
--

INSERT INTO `tenant_states` (`tenant_state_id`, `tenant_state_name`, `tenant_state_status`) VALUES
(1, 'Prospect', 1),
(2, 'Joining', 1),
(3, 'Internal Moving', 1),
(4, 'Moving', 1),
(5, 'Living', 1),
(6, 'Leaving', 1),
(8, 'Accounting', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE IF NOT EXISTS `user_types` (
  `user_type_id` int(11) NOT NULL,
  `user_type_name` varchar(100) NOT NULL,
  `user_type_desc` text NOT NULL,
  `user_type_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`user_type_id`, `user_type_name`, `user_type_desc`, `user_type_status`) VALUES
(1, 'Super Admin', 'Rishi Community team users. Can add and edit all the forms', 1),
(2, 'Admin', 'Roles need to be decided', 1),
(3, 'Marketing/Sales', 'Roles need to be decided', 1),
(4, 'Accountant', 'Roles need to be decided', 1),
(5, 'EndUser', 'EndUser Roles need to be decided', 1),
(6, 'Prospect User', 'Prospect user Roles need to be decided', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`about_id`);

--
-- Indexes for table `accomodation_types`
--
ALTER TABLE `accomodation_types`
  ADD PRIMARY KEY (`accomodation_type_id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `apartments`
--
ALTER TABLE `apartments`
  ADD PRIMARY KEY (`apt_id`);

--
-- Indexes for table `bill_types`
--
ALTER TABLE `bill_types`
  ADD PRIMARY KEY (`bill_type_id`);

--
-- Indexes for table `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`career_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `discount_coupons`
--
ALTER TABLE `discount_coupons`
  ADD PRIMARY KEY (`discount_coupon_id`);

--
-- Indexes for table `jobseekers`
--
ALTER TABLE `jobseekers`
  ADD PRIMARY KEY (`jobseeker_id`);

--
-- Indexes for table `jobseekers_admin_comments`
--
ALTER TABLE `jobseekers_admin_comments`
  ADD PRIMARY KEY (`jobseekers_admin_comment_id`);

--
-- Indexes for table `leasings`
--
ALTER TABLE `leasings`
  ADD PRIMARY KEY (`lease_id`);

--
-- Indexes for table `rents`
--
ALTER TABLE `rents`
  ADD PRIMARY KEY (`rent_id`);

--
-- Indexes for table `rents_for_users`
--
ALTER TABLE `rents_for_users`
  ADD PRIMARY KEY (`rents_for_user_id`);

--
-- Indexes for table `tenants`
--
ALTER TABLE `tenants`
  ADD PRIMARY KEY (`tenant_id`);

--
-- Indexes for table `tenant_apt_data`
--
ALTER TABLE `tenant_apt_data`
  ADD PRIMARY KEY (`tenant_apt_data_id`);

--
-- Indexes for table `tenant_states`
--
ALTER TABLE `tenant_states`
  ADD PRIMARY KEY (`tenant_state_id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`user_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `about_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `accomodation_types`
--
ALTER TABLE `accomodation_types`
  MODIFY `accomodation_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `apartments`
--
ALTER TABLE `apartments`
  MODIFY `apt_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `bill_types`
--
ALTER TABLE `bill_types`
  MODIFY `bill_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `careers`
--
ALTER TABLE `careers`
  MODIFY `career_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `contact_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `discount_coupons`
--
ALTER TABLE `discount_coupons`
  MODIFY `discount_coupon_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jobseekers`
--
ALTER TABLE `jobseekers`
  MODIFY `jobseeker_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `jobseekers_admin_comments`
--
ALTER TABLE `jobseekers_admin_comments`
  MODIFY `jobseekers_admin_comment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `leasings`
--
ALTER TABLE `leasings`
  MODIFY `lease_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `rents`
--
ALTER TABLE `rents`
  MODIFY `rent_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `rents_for_users`
--
ALTER TABLE `rents_for_users`
  MODIFY `rents_for_user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tenants`
--
ALTER TABLE `tenants`
  MODIFY `tenant_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tenant_apt_data`
--
ALTER TABLE `tenant_apt_data`
  MODIFY `tenant_apt_data_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tenant_states`
--
ALTER TABLE `tenant_states`
  MODIFY `tenant_state_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `user_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
